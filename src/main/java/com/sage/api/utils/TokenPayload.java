package com.sage.api.utils;

import lombok.Data;

@Data
public class TokenPayload {

    /**
     * IGNORE,
     * B|Link internal
     */
    public String token_id;

    /**
     * REQUIRED
     * Issued at Date
     */
    public long iat;

    /**
     * REQUIRED, VALIDATE
     * expressed in seconds from epoc
     * Date date = new Date(payload.exp * 1000)
     */
    public long exp;

    /**
     * REQUIRED, VALIDATE
     * MAC ADDRESS or any other MACHINE unique identifier
     */
    public String machine_id;

    /**
     * OPTIONAL, IGNORE
     * B|Link internal
     */
    public String client_id;

    /**
     * OPTIONAL, IGNORE
     * B|Link internal
     */
    public String client_name;
    /**
     * REQUIRED, VALIDATE max concurrent logins
     */
    public int max_logins;

    /**
     * REQUIRED, VALIDATE
     * Issuer: licensing.brilliantlink.app
     */
    public String iss;

    /**
     * IGNORE, reserved for future use.
     * Absence of vlues allows all.
     * this app internal name
     */
    public String aud;

    /**
     * REQUIRED, validate
     * validator": "https://licensing.brillianbtlink.ap/v1/validate",
     * URL to validate the TOKEN
     */
    public String validator;

    /**
     * IGNORE, reserved for future use
     * max_databases
     */
    public int max_databases;

    /**
     * IGNORE, reserved for future use.
     * Absence of vlues allows all.
     */
    public String[] sql_server;

    /**
     * IGNORE, reserved for future use.
     * Absence of vlues allows all.
     */
    public String[] evolution_agents;
}
