package com.sage.api.service.login;

import com.sage.api.domain.login.Agent;
import com.sage.api.domain.login.ForgotPassword;
import com.sage.api.repository.login.AgentLoginRepository;
import com.sage.api.utils.FreedomServicesConstants;
import com.sage.api.utils.LicenseParser;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service(value = "userService")
public class AgentLoginServiceImpl implements AgentLoginService {

    private static final Logger log = LogManager.getLogger(AgentLoginServiceImpl.class);
    @Autowired
    private AgentLoginRepository loginServiceRepo;
    @Autowired
    private LicenseParser licenseParser;
    @Autowired
    private JavaMailSender javaMailSender;
    private ResponseDomain responseDomain;

    /**
     *
     * FETCHING ALL AGENTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> getAgents() {
        log.info("Entering Service Class :::: AgentLoginServiceImpl :::: method :::: getAgents");
        return loginServiceRepo.fetchingAllAgents();
    }

    /**
     *
     * AGENT AUTHENTICATION(LOGIN)
     *
     *
     **/
    @Override
    public ResponseEntity<?> agentAuthentication(String name, String password, String deviceType) {
        log.info("Entering Service Class :::: AgentLoginServiceImpl :::: method :::: agentAuthentication");
       /* String licenceCheck=registrationCheck();
        if (!licenceCheck.equals("Authenticated Successfully")) {
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.UNAUTHORIZED,new Date(System.currentTimeMillis()),""+licenceCheck,false), HttpStatus.UNAUTHORIZED);
        }
        ResponseEntity registrationValidator=licenseValidator();*/
     /*   if (registrationValidator.getStatusCode().value() != 200) {
            log.info("Exiting Service Class :::: AgentLoginServiceImpl :::: method :::: agentAuthentication");
            return ResponseDomain.badRequest( "License validation unsuccessful: Please contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za",false);
        }*/
        String serviceUrl = FreedomServicesConstants.loginApi + "/Admin/Rest/AgentExists";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Username", name);
        headers.add("Password", password);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        try {
            restTemplate.exchange(serviceUrl, HttpMethod.POST, entity, String.class);
            log.info("User Authenticated Successfully through Freedom Service");
            return loginServiceRepo.agentAuthentication(name, deviceType, password);
        } catch (Exception e) {
            log.error("User Authentication failed through Freedom service " + e);
            log.info("Exiting Service Class :::: AgentLoginServiceImpl :::: method :::: agentAuthentication :::: Error :::: "+e);
            return ResponseDomain.badRequest("Wrong Credentials",false);
        }
    }

    /**
     *
     * FETCHING AGENT DETAILS
     *
     *
     **/
    public Agent loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("Entering Service Class :::: Service Class :::: AgentLoginServiceImpl :::: method :::: loadUserByUsername");
        Agent user = loginServiceRepo.findByUserName(s);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        log.info("Exiting Service Class :::: Service Class :::: AgentLoginServiceImpl :::: method :::: loadUserByUsername");
        return user;
    }

    /**
     *
     * VALIDATING LICENSE
     *
     *
     **/
    public String registrationCheck() {
        log.info("Entering Service Class :::: Service Class :::: AgentLoginServiceImpl :::: method :::: registrationCheck");
        String license = loginServiceRepo.fetchingLicense();
        if (license.contains("Error")) {
            log.info("Exiting Service Class :::: Service Class :::: AgentLoginServiceImpl :::: method :::: registrationCheck");
            return "No license Available: Please contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za";
        }
        return licenseParser.parsingToken(license);
    }

    /**
     *
     * VALIDATING LICENSE THROUGH FREEDOM SERVICE
     *
     *
     **/
    public ResponseEntity<?> licenseValidator(){
        log.info("Entering Service Class :::: AgentLoginServiceImpl :::: method :::: licenseValidator");
        List<String> secretKey = licenseParser.fetchingValidator();
        RestTemplate restTemplate =  new RestTemplate();
        log.info(secretKey.get(0)+" ----------------------- "+secretKey.get(1));
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("License", "Lic "+secretKey.get(0));
        headers.add("Accept", "application/json");
        HttpEntity<?> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<?> response = restTemplate.exchange(secretKey.get(1), HttpMethod.GET, entity, String.class);
            log.info("Account Authenticated Successfully through Freedom Service");
            return response;
        } catch (Exception e) {
            log.error("Account Authentication failed through Freedom service " + e);
            log.info("Exiting Service Class :::: AgentLoginServiceImpl :::: method :::: licenseValidator :::: Error :::: "+e);
            return new ResponseEntity<>(new ResponseDomain(false, "License Authentication Failed Please Contact to Administrator"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     *
     * RESET PASSWORD
     *
     *
     **/
    @Override
    public ResponseEntity<?> resetPassword(ForgotPassword agentDetails) {
        log.info("Entering Service Class :::: AgentLoginServiceImpl :::: method :::: resetPassword");
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date date = new Date();
        if (!isValid(agentDetails.getAgentEmail()))
            return ResponseDomain.badRequest("Please enter valid agent email", false);
        else if (!isValid(agentDetails.getMangerEmail()))
            return ResponseDomain.badRequest("Please enter valid manager email", false);
        else {
            try {
                List<String> details = loginServiceRepo.fetchingDBName(agentDetails.getAgentName());
                if (details.contains("") || details.contains("error") || details.contains(null))
                    return ResponseDomain.internalServerError("Admin/Agent mail is empty",false );
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(details.get(2));
                message.setFrom(agentDetails.getAgentEmail());
                message.setCc(new String[]{agentDetails.getMangerEmail(),agentDetails.getAgentEmail(),details.get(0)});
                message.setBcc("admin@brilliantlink.co.za");
                message.setSubject(" Reset Password Request for " + agentDetails.getAgentName() + " on " +details.get(1));
                message.setText("The following Password Reset was requested from:\n\n" + " User:\t" + agentDetails.getAgentName() + "\nDB_Name:\t" + details.get(1) + "\nManager:\t" + agentDetails.getMangerEmail() + "\nDate Logged:\t" + df.format(date) + "\n\n\nRegards\n" + "DB Administrator");
                javaMailSender.send(message);
                log.info("Exiting Service Class :::: AgentLoginServiceImpl :::: method :::: resetPassword");
                return new ResponseEntity(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "Mail Sent Successfully", true), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Exiting Service Class :::: AgentLoginServiceImpl :::: method :::: resetPassword :::: Error :::: "+e);
                return ResponseDomain.internalServerError("Internal Server Error", false);
            }
        }
    }

    /**
     *
     * VALIDATING EMAIL
     *
     *
     **/
    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

}



