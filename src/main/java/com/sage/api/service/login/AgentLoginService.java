package com.sage.api.service.login;

import com.sage.api.domain.login.ForgotPassword;
import org.springframework.http.ResponseEntity;

public interface AgentLoginService {

    ResponseEntity<?> getAgents();

    ResponseEntity<?> agentAuthentication(String name,String password, String deviceType);

    ResponseEntity<?> resetPassword(ForgotPassword agentDetails);
}
