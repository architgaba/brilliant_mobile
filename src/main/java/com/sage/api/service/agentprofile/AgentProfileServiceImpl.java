package com.sage.api.service.agentprofile;

import com.sage.api.repository.agentprofile.AgentProfileRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AgentProfileServiceImpl implements  AgentProfileService {
    private static final Logger log = LogManager.getLogger(AgentProfileServiceImpl.class);
    @Autowired
    private AgentProfileRepository agentProfileRepository;

    /*
     *
     * UPLOADING AGENT PROFILE IMAGE
     *
     *
     */
    @Override
    public ResponseEntity<?> uploadingProfileImage(String image, String agentName,String device) {
        log.info("Entering Service Class :::: AgentProfileServiceImpl :::: method :::: uploadingProfileImage :::: Agent :::: " + agentName);
        return agentProfileRepository.uploadingAgentProfileImage(image, agentName,device);
    }

    /*
     *
     *FETCHING AGENT PROFILE IMAGE
     *
     *
     */
    @Override
    public ResponseEntity<?> fetchingProfileImage(String agentName,String device) {
        log.info("Entering Service Class :::: AgentProfileServiceImpl :::: method :::: fetchingProfileImage :::: Agent :::: "+agentName);
        return agentProfileRepository.fetchingAgentProfileImage(agentName,device);
    }
}
