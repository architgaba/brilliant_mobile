package com.sage.api.service.receipts;

import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.domain.receipts.Transactions;
import org.springframework.http.ResponseEntity;

public interface ReceiptsService {

    ResponseEntity<?> customerAccounts();

    ResponseEntity<?> invoicePay(Transactions transactions, String[] mode , Float[] amount,String authKey,String POSChange, String POSTender);

    ResponseEntity<?> agentReceipts(Accounts account, String agent);
}
