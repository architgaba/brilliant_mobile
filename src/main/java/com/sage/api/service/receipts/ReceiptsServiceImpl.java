package com.sage.api.service.receipts;

import com.google.gson.Gson;
import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.domain.receipts.Transactions;
import com.sage.api.domain.salesorder.salesordertransaction.Receipts;
import com.sage.api.repository.receipts.ReceiptsRepository;
import com.sage.api.utils.Error;
import com.sage.api.utils.FreedomServicesConstants;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.ValidationErrors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.Date;

@Service
public class ReceiptsServiceImpl implements ReceiptsService {
    private static final Logger log = LogManager.getLogger(ReceiptsServiceImpl.class);
    @Autowired
    private ReceiptsRepository receiptsRepository;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * FETCHING ALL CUSTOMER ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> customerAccounts() {
        log.info("Entering Service Class :::: ReceiptsServiceImpl :::: method :::: customerAccounts");
        return receiptsRepository.fetchCustomerAccount();
    }


    /**
     *
     * INVOICE PAYMENT
     *
     *
     **/
    @Override
    public synchronized ResponseEntity<?> invoicePay(Transactions transactions, String[] mode, Float[] amount, String authKey, String POSChange, String POSTender) {
        log.info("Entering Service Class :::: ReceiptsServiceImpl :::: method :::: invoicePay");
        String finalResult = "", result = "";
        for (int i = 0; i < mode.length; i++) {
            Receipts receipts = transactions.getTransac();
            if (amount[i] != null) {
                receipts.setReference2(mode[i]);
                receipts.setAmount(amount[i]);
                transactions.setTransac(receipts);
                result = invoicePayment(transactions, authKey);
                if (!result.contains("true")) {
                    receiptsRepository.updatingPOSTAR(jwtTokenUtil.getAgentIdFromToken(authKey), receipts.getCustomerCode());
                    if (receipts.getReference() != "0")
                        receiptsRepository.updatingInvNum(POSTender, POSChange, receipts.getReference());
                }
                finalResult = result + finalResult;
            }
        }
        if (finalResult.contains("true")) {
            Gson g = new Gson();
            Error apiResponseError = g.fromJson(result, Error.class);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.APPLICATION_JSON);
            String message = "";
            if (!apiResponseError.getValidationErrors().isEmpty()) {
                for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                    message = Arrays.toString(err.getErrors());
                }
                log.info("Exiting Service Class :::: ReceiptsServiceImpl :::: method :::: invoicePay");
                return ResponseDomain.internalServerError( "" + message,false);
            }
        }
        log.info("Exiting Service Class :::: ReceiptsServiceImpl :::: method :::: invoicePay");
        return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "Payment made Successfully", true), HttpStatus.OK);
    }

    /**
     *
     *
     * CALLING INVOICE PAYMENT FREEDOM SERVICES
     *
     *
     **/
    public synchronized  String invoicePayment(Transactions transc, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: invoicePayment");
        String apiUrl = FreedomServicesConstants.baseUrl + "/CustomerTransactionPost";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        HttpEntity<Object> entity = new HttpEntity<>(transc, headers);
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: invoicePayment");
            return "" + response.getBody();
        } catch (Exception e) {
            log.error("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: invoicePayment :::: Error :::: " + e);
            return "" + e;
        }
    }

    /**
     *
     *
     * FETCHING AGENT RECEIPTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> agentReceipts(Accounts account, String agent) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: agentReceipts");
        return receiptsRepository.agentReceiptsData(account,agent);
    }
}
