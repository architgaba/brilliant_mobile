package com.sage.api.service.template;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.template.*;
import com.sage.api.repository.salesorder.salesordertransaction.SalesOrderTransactionRepository;
import com.sage.api.repository.template.TemplateCreator;
import com.sage.api.repository.template.TemplateRepository;
import com.sage.api.utils.JwtConstants;
import com.sage.api.utils.PDFConverter;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.*;
import java.time.LocalDate;
import java.util.*;

@Service
public class TemplateServiceImpl implements TemplateService {

    private static final Logger log = LogManager.getLogger(TemplateServiceImpl.class);
    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private PDFConverter pdfConverter;
    @Autowired
    private TemplateCreator templateCreator;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JavaMailSender javaMailSender;


    /**
     * UPDATING TEMPLATE
     **/
    @Override
    public ResponseEntity<?> updateTemplate(SavedTemplate savedTemplate) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate");
        try {
            List<SavedTemplate> templateList = templateRepository.findByDocId(savedTemplate.getDocId());
            SavedTemplate templateObj = templateList.get(0);
            templateObj.setTemplate(savedTemplate.getTemplate());
            templateRepository.save(templateObj);
            pdfConverter.generateHTML(savedTemplate.getTemplate(), generatePOJO(savedTemplate.getDocId().toString()), savedTemplate.getDocId().toString());
            pdfConverter.convertTOPDF(savedTemplate.getTemplate(), savedTemplate.getDocId().toString());
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate");
            return ResponseDomain.successResponse("template updated successfully", true);
        } catch (Exception e) {
            e.printStackTrace();
            templateRepository.save(savedTemplate);
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Error Occurred in updating template", false);
        }
    }

    /**
     * DOWNLOADING TEMPLATE (SALES ORDER)
     **/
    @Override
    public ResponseEntity<?> downloadPdf(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf");
        String currentDirectory = System.getProperty("user.dir");
        File f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        File f1 = new File(currentDirectory + "\\generatedHTML\\" + docId + ".html");

        if (!f.exists() && !f1.exists()) {
            viewTemplate(docId, docState);
            f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        }
        try {
            byte[] data = pdfConverter.downloadPdf(f, docId);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf");
            if (f1.exists()) {
                f1.delete();
                f.delete();
            }
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } catch (Exception e1) {
            e1.printStackTrace();
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf :::: Error :::: " + e1);
            return ResponseDomain.internalServerError("Server Error", false);
        }
    }

    /**
     * DOWNLOADING TEMPLATE (PURCHASE ORDER)
     **/
    @Override
    public ResponseEntity<?> downloadPdfForPurchaseOrder(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: downloadPdfForPurchaseOrder");
        String currentDirectory = System.getProperty("user.dir");
        File f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        File f1 = new File(currentDirectory + "\\generatedHTML\\" + docId + ".html");
        if (!f.exists()) {
            viewTemplateHtmlForPurchaseOrder(docId, docState);
            f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        }
        try {
            byte[] data = pdfConverter.downloadPdf(f, docId);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            if (f1.exists()) {
                f1.delete();
                f.delete();
            }
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdfForPurchaseOrder");
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } catch (Exception e1) {
            e1.printStackTrace();
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdfForPurchaseOrder :::: Error " + e1);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }


    /**
     * GENERATING POJO FOR TEMPLATE (SALES ORDER)
     **/
    public TemplatePOJO generatePOJO(String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: generatePOJO");
        TemplatePOJO templatePOJO = templateCreator.templateData(docId);
        TemplateOrderTotals orderTotals = templateCreator.templateOrderData(templatePOJO.getDocState(), docId);
        List<TemplateLines> templateLines = templateCreator.templateLinesData(docId, templatePOJO.getRepCode(), templatePOJO.getDocState());
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        List<String> tableHeader = new ArrayList<String>();
        Lines lines = new Lines();
        lines.setTableRow(templateLines);
        templatePOJO.setTotalIncl(String.format("%.2f", Float.parseFloat(orderTotals.getTotalIncl())));
        templatePOJO.setTotalExc(String.format("%.2f", Float.parseFloat(orderTotals.getTotalExc())));
        templatePOJO.setTax(String.format("%.2f", Float.parseFloat(orderTotals.getTax())));

        templatePOJO.setDiscount(String.format("%.2f", Float.parseFloat(orderTotals.getDiscount())));

        templatePOJO.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalInclFinal())));
        String docStateCheck = templatePOJO.getDocState();
        switch (docStateCheck) {
            case "Quotation":
                templatePOJO.setDocState("Quotation");
                break;
            case "Archived":
                templatePOJO.setDocState("Tax Invoice");
                break;
            case "Archived Quotation":
                templatePOJO.setDocState("Archived Quotation");
                break;
            case "Partially Processed":
                templatePOJO.setDocState("Sales Order Confirmation");
                break;
            case "Unprocessed":
                templatePOJO.setDocState("Sales Order Confirmation");
                break;
        }
        if ("Quotation".equals(templatePOJO.getDocState()) || "Tax Invoice".equals(templatePOJO.getDocState()) || "Archived Quotation".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Units");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        } else if ("Sales Order Confirmation".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Qty Ordered");
            tableHeader.add("Qty Processed");
            tableHeader.add("Qty To Invoice");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        }
        lines.setFieldNames(tableHeader);
        templatePOJO.setLines(lines);

        orderTotals.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalIncl())));

        templatePOJO.setTotalIncl(String.format("%.2f", Float.parseFloat(orderTotals.getTotalIncl())));
        templatePOJO.setTotalExc(String.format("%.2f", Float.parseFloat(orderTotals.getTotalExc())));
        templatePOJO.setTax(String.format("%.2f", Float.parseFloat(orderTotals.getTax())));
        templatePOJO.setDiscount(String.format("%.2f", Float.parseFloat(orderTotals.getDiscount())));
        templatePOJO.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalInclFinal())));
        String fetchingsignaturedetails = templateCreator.fetchingSignatureDetails(docId);
        templatePOJO.setSignature(fetchingsignaturedetails);
        templatePOJO.setCurrentdate(LocalDate.now().toString());
        if (templatePOJO.getRepCode() == null)
            templatePOJO.setRepCode("");

        List<TemplateLines> tableRow = templatePOJO.getLines().getTableRow();

        tableRow.forEach(line -> {
            if (line.getRep() == null)
                line.setRep("");
        });
        if (templatePOJO.getTelephone() == null)
            templatePOJO.setTelephone("");

        if (templatePOJO.getContactPerson() == null)
            templatePOJO.setContactPerson("");

        if (templatePOJO.getFax() == null)
            templatePOJO.setFax("");

        if (templatePOJO.getEmail1() == null)
            templatePOJO.setEmail1("");

        if (templatePOJO.getPo1() == null)
            templatePOJO.setPo1("");

        if (templatePOJO.getPo2() == null)
            templatePOJO.setPo2("");

        if (templatePOJO.getPo3() == null)
            templatePOJO.setPo3("");

        if (templatePOJO.getPa1() == null)
            templatePOJO.setPa1("");

        if (templatePOJO.getPa2() == null)
            templatePOJO.setPa2("");

        if (templatePOJO.getPa3() == null)
            templatePOJO.setPa3("");

        if (templatePOJO.getPa4() == null)
            templatePOJO.setPa4("");

        if (templatePOJO.getSignature() == null)
            templatePOJO.setSignature("");
        log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: generatePOJO");
        return templatePOJO;
    }

    /**
     * GENERATING POJO FOR TEMPLATE (PURCHASE ORDER)
     **/
    public TemplatePOJO generatePOJOForPurchaseOrder(String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: generatePOJOForPurchaseOrder");
        TemplatePOJO templatePOJO = templateCreator.templateSupplierData(docId);
        TemplateOrderTotals orderTotals = templateCreator.templateOrderData(templatePOJO.getDocState(), docId);
        List<TemplateLines> templateLines = templateCreator.templateLinesData(docId, templatePOJO.getRepCode(), templatePOJO.getDocState());
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        List<String> tableHeader = new ArrayList<String>();
        Lines lines = new Lines();
        lines.setTableRow(templateLines);
        templatePOJO.setTotalIncl(String.format("%.2f", Float.parseFloat(orderTotals.getTotalIncl())));
        templatePOJO.setTotalExc(String.format("%.2f", Float.parseFloat(orderTotals.getTotalExc())));
        templatePOJO.setTax(String.format("%.2f", Float.parseFloat(orderTotals.getTax())));
        templatePOJO.setDiscount(String.format("%.2f", Float.parseFloat(orderTotals.getDiscount())));
        templatePOJO.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalInclFinal())));
        String docStateCheck = templatePOJO.getDocState();
        switch (docStateCheck) {
            case "Quotation":
                templatePOJO.setDocState("Quotation");
                break;
            case "Archived":
                templatePOJO.setDocState("Tax Invoice");
                break;
            case "Partially Processed":
                templatePOJO.setDocState("Purchase Order Confirmation");
                break;
            case "Unprocessed":
                templatePOJO.setDocState("Purchase Order Confirmation");
                break;
        }
        if ("Quotation".equals(templatePOJO.getDocState()) || "Tax Invoice".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Units");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");
        } else if ("Purchase Order Confirmation".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Qty Ordered");
            tableHeader.add("Qty Processed");
            tableHeader.add("Qty To Invoice");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");
        }
        lines.setFieldNames(tableHeader);
        templatePOJO.setLines(lines);
        orderTotals.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalInclFinal())));
        templatePOJO.setTotalIncl(String.format("%.2f", Float.parseFloat(orderTotals.getTotalIncl())));
        templatePOJO.setTotalExc(String.format("%.2f", Float.parseFloat(orderTotals.getTotalExc())));
        templatePOJO.setTax(String.format("%.2f", Float.parseFloat(orderTotals.getTax())));
        templatePOJO.setDiscount(String.format("%.2f", Float.parseFloat(orderTotals.getDiscount())));
        templatePOJO.setTotalInclFinal(String.format("%.2f", Float.parseFloat(orderTotals.getTotalInclFinal())));
        templatePOJO.setSignature(templateCreator.fetchingSignatureDetails(docId));
        templatePOJO.setCurrentdate(LocalDate.now().toString());
        if (templatePOJO.getRepCode() == null)
            templatePOJO.setRepCode("");
        List<TemplateLines> tableRow = templatePOJO.getLines().getTableRow();
        tableRow.forEach(line -> {
            if (line.getRep() == null)
                line.setRep("");
        });
        if (templatePOJO.getTelephone() == null)
            templatePOJO.setTelephone("");

        if (templatePOJO.getContactPerson() == null)
            templatePOJO.setContactPerson("");

        if (templatePOJO.getFax() == null)
            templatePOJO.setFax("");

        if (templatePOJO.getEmail1() == null)
            templatePOJO.setEmail1("");

        if (templatePOJO.getPo1() == null)
            templatePOJO.setPo1("");

        if (templatePOJO.getPo2() == null)
            templatePOJO.setPo2("");

        if (templatePOJO.getPo3() == null)
            templatePOJO.setPo3("");

        if (templatePOJO.getPa1() == null)
            templatePOJO.setPa1("");

        if (templatePOJO.getPa2() == null)
            templatePOJO.setPa2("");

        if (templatePOJO.getPa3() == null)
            templatePOJO.setPa3("");

        if (templatePOJO.getPa4() == null)
            templatePOJO.setPa4("");

        if (templatePOJO.getSignature() == null)
            templatePOJO.setSignature("");
        log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: generatePOJOForPurchaseOrder");
        return templatePOJO;
    }

    /**
     * VIEWING TEMPLATE (SALES ORDER)
     **/
    @Override
    public ResponseEntity<?> viewTemplate(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate");
        if (docState.contains(" "))
            docState = docState.replace(" ", "_");
        SavedTemplate template = null;
        try {
            String path = System.getProperty("user.dir");
            path = path + "\\src\\main\\resources\\" + docState + ".html";
            String GenericTemplate = new String(Files.readAllBytes(Paths.get
                    (path)));
            TemplatePOJO temp = generatePOJO(docId);
            String convertedTemplate = pdfConverter.generateHTML(GenericTemplate, generatePOJO(docId), docId);
            SavedTemplate savedTemplateObj = new SavedTemplate();
            savedTemplateObj.setTemplate(convertedTemplate);
            savedTemplateObj.setDocId(Long.parseLong(docId));
            savedTemplateObj.setDocState(docState);
            /* templateRepository.save(savedTemplateObj);*/
            pdfConverter.convertTOPDF(convertedTemplate, docId);
            /*   viewTemplate(docId, docState);*/
            template = savedTemplateObj;
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate");
            return ResponseDomain.successResponse("" + template.getTemplate(), true);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * VIEWING TEMPLATE (PURCHASE ORDER)
     **/
    @Override
    public ResponseEntity<?> viewTemplateHtmlForPurchaseOrder(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: viewTemplateHtmlForPurchaseOrder");
              List<SavedTemplate> savedTemplate = templateRepository.findByDocId(Long.parseLong(docId));
        if (docState.contains(" "))
            docState = docState.replace(" ", "_");
        SavedTemplate template = null;
        try {
            String path = System.getProperty("user.dir");
            path = path + "\\src\\main\\resources\\" + docState + ".html";
            String GenericTemplate = new String(Files.readAllBytes(Paths.get
                    (path)));
            String convertedTemplate = pdfConverter.generateHTML(GenericTemplate, generatePOJOForPurchaseOrder(docId), docId);
            SavedTemplate savedTemplateObj = new SavedTemplate();
            savedTemplateObj.setTemplate(convertedTemplate);
            savedTemplateObj.setDocId(Long.parseLong(docId));
            savedTemplateObj.setDocState(docState);
            pdfConverter.convertTOPDF(convertedTemplate, docId);
            template = savedTemplateObj;
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplateHtmlForPurchaseOrder");
            return ResponseDomain.successResponse("" + template.getTemplate(), true);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplateHtmlForPurchaseOrder :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * DEFAULT EMAIL DATA (PURCHASE ORDER)
     **/
    @Override
    public ResponseEntity<?> mailDataForPurchaseOrder(String account, String authKey) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailDataForPurchaseOrder");
        return templateCreator.defaultSupplierMail(jwtTokenUtil.getUsernameFromToken(authKey), account);
    }

    /**
     * DEFAULT EMAIL DATA(SALES ORDER)
     **/
    @Override
    public ResponseEntity<?> mailData(String account, String authKey, String docState, String orderNum) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailData");
        return templateCreator.defaultMail(jwtTokenUtil.getUsernameFromToken(authKey), account, docState, orderNum);
    }

    /**
     * GENERATING EMAIL DATA(SALES ORDER)
     **/
    @Override
    public ResponseEntity<?> generateReceipt(ReceiptData receiptData, String authKey) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: generateReceipt");
        String currentDirectory = System.getProperty("user.dir");
        String path = "";
        try {
            ReceiptPOJO receiptPOJO = new ReceiptPOJO();
            receiptPOJO.setAccount(receiptData.getAccount());
            receiptPOJO.setCustomerName(receiptData.getCustomerName());
            receiptPOJO.setDate(receiptData.getDate());
            receiptPOJO.setTill(receiptData.getTill());
            receiptPOJO.setDescription(receiptData.getDescription());
            receiptPOJO.setAmountPaid(receiptData.getAmountPaid().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getAmountPaid())));
            receiptPOJO.setCash(receiptData.getCash().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getCash())));
            receiptPOJO.setCard(receiptData.getCard().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getCard())));
            receiptPOJO.setEft(receiptData.getEft().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getEft())));
            receiptPOJO.setOther(receiptData.getOther().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getOther())));
            receiptPOJO.setTotal(receiptData.getTotal().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getTotal())));
            receiptPOJO.setBalanceAmount(receiptData.getBalanceAmount().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getBalanceAmount())));
            receiptPOJO.setChangeAmount(receiptData.getChangeAmount().length() == 0 ? "" : String.format("%.2f", Float.parseFloat(receiptData.getChangeAmount())));
            AccountDetails accountDetails = templateCreator.customerDetails(receiptData.getCustomerName());
            receiptPOJO.setPa1(accountDetails.getAddress1());
            receiptPOJO.setPa2(accountDetails.getAddress2());
            receiptPOJO.setPa3(accountDetails.getAddress3() + "  " + accountDetails.getAddress4());
            receiptPOJO.setPa4(accountDetails.getAddress5() + "  " + accountDetails.getAddress6());
            receiptPOJO.setPo1(accountDetails.getPAddress1());
            receiptPOJO.setPo2(accountDetails.getPAddress2() + "  " + accountDetails.getPAddress3() + "  " + accountDetails.getPAddress4());
            receiptPOJO.setPo3(accountDetails.getPAddress5() + "  " + accountDetails.getPAddress6());
            receiptPOJO.setTelephone(accountDetails.getTelephone());
            receiptPOJO.setEmail1(accountDetails.getEmail());
            receiptPOJO.setContactPerson(accountDetails.getContactPerson());
            if (receiptPOJO.getContactPerson() == null)
                receiptPOJO.setContactPerson("");
            if (receiptPOJO.getTelephone() == null)
                receiptPOJO.setTelephone("");
            if (receiptPOJO.getEmail1() == null)
                receiptPOJO.setEmail1("");
            if (receiptPOJO.getPo1() == null)
                receiptPOJO.setPo1("");
            if (receiptPOJO.getPo2() == null)
                receiptPOJO.setPo2("");
            if (receiptPOJO.getPo3() == null)
                receiptPOJO.setPo3("");
            if (receiptPOJO.getPa1() == null)
                receiptPOJO.setPa1("");
            if (receiptPOJO.getPa2() == null)
                receiptPOJO.setPa2("");
            if (receiptPOJO.getPa3() == null)
                receiptPOJO.setPa3("");
            if (receiptPOJO.getPa4() == null)
                receiptPOJO.setPa4("");
            path = currentDirectory + "\\src\\main\\resources\\ReceiptTemplate.html";
            String GenericTemplate = new String(Files.readAllBytes(Paths.get
                    (path)));
            pdfConverter.generateHTML(GenericTemplate, receiptPOJO, "Receipt(" + jwtTokenUtil.getUsernameFromToken(authKey) + "_" + jwtTokenUtil.getDeviceTypeFromToken(authKey) + ")");
            pdfConverter.convertTOPDF("", "Receipt(" + jwtTokenUtil.getUsernameFromToken(authKey) + "_" + jwtTokenUtil.getDeviceTypeFromToken(authKey) + ")");
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: generateReceipt");
            return ResponseDomain.successResponse("Receipt Generated Successfully", true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Server Error in Generating Receipt", true);
        }
    }

    /**
     * EMAILING TEMPLATE (PURCHASE ORDER)
     **/
    @Override
    public ResponseEntity<?> mailSenderForPurchaseOrder(MailSender sender, String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailSenderForPurchaseOrder");
        String pdfDirectory = System.getProperty("user.dir") + "\\generatedPDF\\";
        if (!new File(pdfDirectory + docId + ".pdf").exists()) {
            String docState = templateCreator.docState(docId);
            viewTemplateHtmlForPurchaseOrder(docId, docState);
        }
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(mimeMessage, "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(sender.getTo());
            helper.setFrom(sender.getFrom());
            helper.setSubject(sender.getSubject());
            if (sender.getBcc() != null && sender.getBcc().length > 0) {
                helper.setBcc(sender.getBcc());
            }
            if (sender.getCc() != null && sender.getCc().length > 0) {
                helper.setCc(sender.getCc());
            }
            helper.setText(sender.getBody());
            FileSystemResource temp = new FileSystemResource(new File(pdfDirectory + docId + ".pdf"));
            helper.addAttachment(sender.getFileName() + ".pdf", temp);
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSenderForPurchaseOrder");
            javaMailSender.send(mimeMessage);
            return ResponseDomain.successResponse("mail Sent Successfully", true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender :::: error occurred :::: " + e);
            return ResponseDomain.internalServerError("Server Error in Emailing Document", true);
        }
    }

    /**
     * EMAILING TEMPLATE(SALES ORDER)
     **/
    @Override
    public ResponseEntity<?> mailSender(MailSender sender, String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailSender");
        String directory = System.getProperty("user.dir");
        String pdfDirectory = System.getProperty("user.dir") + "\\generatedPDF\\";
        File f1 = new File(directory + "\\generatedHTML\\" + docId + ".html");
        File f = new File(directory + "\\generatedPDF\\" + docId + ".pdf");

        if (!f.exists()) {
            String docState = templateCreator.docState(docId);
            viewTemplate(docId, docState);
        }
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(mimeMessage, "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(sender.getTo());
            helper.setFrom(sender.getFrom());
            helper.setSubject(sender.getSubject());
            if (sender.getBcc() != null && sender.getBcc().length > 0) {
                helper.setBcc(sender.getBcc());
            }
            if (sender.getCc() != null && sender.getCc().length > 0) {
                helper.setCc(sender.getCc());
            }
            helper.setText(sender.getBody());
            FileSystemResource temp = new FileSystemResource(new File(pdfDirectory + docId + ".pdf"));
            helper.addAttachment(sender.getFileName() + ".pdf", temp);
            javaMailSender.send(mimeMessage);
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender");
            if (f1.exists()) {
                f1.delete();
                f.delete();
            }
            return ResponseDomain.successResponse("mail Sent Successfully", true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * DOWNLOADING RECEIPT TEMPLATE
     **/
    @Override
    public ResponseEntity<?> downloadReceipt(String key) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: downloadReceipt");
        String currentDirectory = System.getProperty("user.dir");
        try {
            File f = new File(currentDirectory + "\\generatedPDF\\" + "Receipt(" + jwtTokenUtil.getUsernameFromToken(key) + "_" + jwtTokenUtil.getDeviceTypeFromToken(key) + ").pdf");
            byte[] data = pdfConverter.downloadPdf(f, "Receipt( " + jwtTokenUtil.getUsernameFromToken(key) + " )");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadReceipt");
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } catch (Exception e) {
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadReceipt");
            return ResponseDomain.internalServerError("Server Error", false);
        }
    }

    /**
     * API FOR EMAILING RECEIPT TEMPLATE
     **/
    @Override
    public synchronized ResponseEntity<?> mailReceipt(MailSender sender, String token) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailReceipt");
        String directory = System.getProperty("user.dir") + "\\generatedPDF\\";
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(mimeMessage, "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(sender.getTo());
            helper.setFrom(sender.getFrom());
            helper.setSubject(sender.getSubject());
            if (sender.getBcc() != null && sender.getBcc().length > 0) {
                helper.setBcc(sender.getBcc());
            }
            if (sender.getCc() != null && sender.getCc().length > 0) {
                helper.setCc(sender.getCc());
            }
            helper.setText(sender.getBody());
            log.info(directory + "\\generatedPDF\\" + "Receipt(" + jwtTokenUtil.getUsernameFromToken(token) + "_" + jwtTokenUtil.getDeviceTypeFromToken(token) + ").pdf");
            FileSystemResource temp = new FileSystemResource(new File(directory + "Receipt(" + jwtTokenUtil.getUsernameFromToken(token) + "_" + jwtTokenUtil.getDeviceTypeFromToken(token) + ").pdf"));
            helper.addAttachment(sender.getFileName() + ".pdf", temp);
            javaMailSender.send(mimeMessage);
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailReceipt");
            return ResponseDomain.successResponse("mail Sent Successfully", true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailReceipt :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * FETCHING GENERIC TEMPLATE
     **/
    @Override
    public ResponseEntity<?> fetchTemplate(String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: fetchTemplate");
        try {
            String path = System.getProperty("user.dir");
            path = path + "\\src\\main\\resources\\" + docState + ".html";
            String templateData = new String(Files.readAllBytes(Paths.get
                    (path)));
            if (!docState.equals("ReceiptTemplate"))
                templateData = templateData.replace(templateData.substring(templateData.indexOf("<!-[FTL STARTS]->") + "<!-[FTL STARTS]->".length(), templateData.indexOf("<!-[FTL ENDS]->")), "(list)");
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: fetchTemplate");
            return ResponseDomain.successResponse("" + templateData, true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: fetchTemplate :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Server Error / " + e, false);
        }
    }

    /**
     * UPDATING GENERIC TEMPLATE
     **/
    @Override
    public ResponseEntity<?> updateGenericTemplate(UpdateGenericTemplate updateGenericTemplate) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: updateGenericTemplate");
        if (updateGenericTemplate.getTemplate().equals("<html><body>undefined</body></html>")) {
            return ResponseDomain.internalServerError("Please don't update the template in source mode", false);
        }
        String path = System.getProperty("user.dir");
        path = path + "\\src\\main\\resources\\" + updateGenericTemplate.getDocState() + ".html";
        File f = null;
        FileWriter fw = null;
        try {
            f = new File(path);
            fw = new FileWriter(f);
            fw.flush();
            String genericTemplate = updateGenericTemplate.getTemplate();

            if (!updateGenericTemplate.getDocState().equals("ReceiptTemplate")) {
                genericTemplate = genericTemplate.replace("(list)", "<!-[FTL STARTS]->\n" +
                        "<br>\n" +
                        "<br>\n" +
                        "\n" +
                        "<table style=\"width:100%\">\n" +
                        "\n" +
                        "\t<thead>\n" +
                        "    \t\t<tr style=\"font-weight: bold;text-decoration: underline;\">\n" +
                        "    \t\t<#list lines.fieldNames as fieldName>\n" +
                        "    \t\t\t<td>${fieldName}</td>\n" +
                        "    \t\t</#list>\n" +
                        "    \t\t</tr>\n" +
                        "    \t</thead>\n" +
                        "\t<tbody>\n" +
                        "\t<#list lines.tableRow as value>\n" +
                        "\t<#assign x=\"${value.docState}\">\n" +
                        "\t\t<tr>\n" +
                        "\n" +
                        "\n" +
                        "            \t\t\t<td>${value.code}</td>\n" +
                        "            \t\t\t<td>${value.itemDescription}</td>\n" +
                        "            \t\t\t<td>${value.rep}</td>\n" +
                        "            \t\t\t<#if x==\"Archived\">\n" +
                        "            \t\t\t\t<td>${value.qtyProcessed}</td>\n" +
                        "            \t\t\t<#elseif x == \"Unprocessed\">\n" +
                        "            \t\t\t\t<td>${value.qtyOrdered}</td>\n" +
                        "                             <td>${value.qtyProcessed}</td>\n" +
                        "                             <td>${value.qtyToInvoice}</td>\n" +
                        "                        <#elseif x == \"Quotation\">\n" +
                        "                             <td>${value.qtyOrdered}</td>\n" +
                        "                        <#elseif x == \"Partially Processed\">\n" +
                        "                               <td>${value.qtyOrdered}</td>\n" +
                        "                               <td>${value.qtyProcessed}</td>\n" +
                        "                               <td>${value.qtyToInvoice}</td>\n" +
                        "                     \t</#if>\n" +
                        "\n" +
                        "\n" +
                        "            \t\t\t<td>${value.exclPrice}</td>\n" +
                        "            \t\t\t<td>${value.discount}</td>\n" +
                        "            \t\t\t<td>${value.tax}</td>\n" +
                        "            \t\t\t<td>${value.inclPrice}</td>\n" +
                        "\n" +
                        "\t\t</tr>\n" +
                        "\t\t</#list>\n" +
                        "\t</tbody>\n" +
                        "</table><!-[FTL ENDS]->");
            }

            fw.write(genericTemplate);
            fw.close();
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: fetchTemplate");
            return ResponseDomain.successResponse("Template Updated Successfully", true);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: fetchTemplate :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error ", false);
        }
    }
}

