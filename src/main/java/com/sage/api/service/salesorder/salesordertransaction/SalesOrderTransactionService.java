package com.sage.api.service.salesorder.salesordertransaction;

import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.domain.template.MailSender;
import org.springframework.http.ResponseEntity;

public interface SalesOrderTransactionService {

    ResponseEntity<?> fetchWarehouse();

    ResponseEntity<?> fetchingLinesDetails(String id,String authKey);
    
    ResponseEntity<?> fetchingCustomerDetails(String name,String docId);

    ResponseEntity<?> fetchingItemCode();

    ResponseEntity<?> fetchCustomers();

    ResponseEntity<?> fetchDocumentDetails(String name,String docId);

    ResponseEntity<?> fetchMoreDocumentDetails(String docId);

    ResponseEntity<?> fetchingStockData(StockData data);

    ResponseEntity<?> createPlaceSalesOrder(WrapperQuotes wrapperQuotes,String authKey);

    ResponseEntity<?> updateExistingOrder(WrapperSalesorder wrapperSalesorder,String authKey);

    ResponseEntity<?>  updateExitingSalesOrder(WrapperOrder wrapperOrder,String authKey);

    ResponseEntity<?> createInvoice(WrapperQuotes wrapperQuotes,String authKey);

    ResponseEntity<?> createSalesOrderQuotes(WrapperQuotes wrapperQuotes,String authKey);

    ResponseEntity<?> deleteLine(String lineId);

    ResponseEntity<?> templateFunctionality(String reference, String action);

    ResponseEntity<?> templateMailer(MailSender sender,String reference);

    ResponseEntity<?> receiptsData(String invoiceNum);

    ResponseEntity<?> salesOrderGLDropdown();

    ResponseEntity<?> updateCustomFields(String orderNum,String docId,AccountDetails details);
}
