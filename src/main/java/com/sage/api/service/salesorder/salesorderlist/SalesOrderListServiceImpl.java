package com.sage.api.service.salesorder.salesorderlist;

import com.sage.api.domain.salesorder.salesorderlist.SalesOrderList;
import com.sage.api.repository.enquiries.accountreceivable.AccountReceivableRepo;
import com.sage.api.repository.salesorder.salesorderlist.SalesOrderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SalesOrderListServiceImpl implements SalesOrderListService {
    private static final Logger log = LogManager.getLogger(SalesOrderListServiceImpl.class);
    @Autowired
    private SalesOrderRepository salesOrderRepository;
    @Autowired
    private AccountReceivableRepo accountReceivableRepo;

    /**
     *
     * FETCHING SALES ORDER DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingSalesOrderListData(SalesOrderList salesOrderList) {
        log.info("Entering Service Class :::: SalesOrderListServiceImpl :::: method :::: fetchingSalesOrderListData");
        return salesOrderRepository.fetchingSalesOrderListData(salesOrderList);
    }

    /**
     *
     * FETCHING ALL CUSTOMER ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity fetchingAccounts() {
        log.info("Entering Service Class :::: SalesOrderListServiceImpl :::: method :::: fetchingAccounts");
        ResponseEntity<?> responseEntity = accountReceivableRepo.fetchingAllAccounts();
        if (responseEntity != null) {
            log.info("Exiting Service Class :::: SalesOrderListServiceImpl :::: method :::: fetchingAccounts");
            return responseEntity;
        }
        log.info("Exiting Service Class :::: SalesOrderListServiceImpl :::: method :::: fetchingAccounts");
        return new ResponseEntity<>("No Client Exists", HttpStatus.OK);
    }
}
