package com.sage.api.service.salesorder.stockcheck;

import com.sage.api.domain.salesorder.stockcheck.InventoryCheck;
import com.sage.api.repository.salesorder.stockcheck.StockCheckRespository;
import com.sage.api.service.enquiries.accountreceivable.AccountReceivableServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StockCheckServiceImpl implements StockCheckService {
    private static final Logger log = LogManager.getLogger(StockCheckServiceImpl.class);

    @Autowired
    private StockCheckRespository inventoryStockCheck;

    /**
     *
     * FETCHING INVENTORY STORE DETAIL
     *
     *
     **/
    @Override
    public ResponseEntity<?> getInventoryStock() {
        log.info("Entering Service Class :::: StockCheckRespository :::: method :::: getInventoryStock");
        return inventoryStockCheck.getInventoryStock();
    }

    /**
     *
     * FETCHING INVENTORY STOCK DETAIL
     *
     *
     **/
    @Override
    public ResponseEntity<?> stockCheck(InventoryCheck inventoryCheck) {
        log.info("Entering Service Class :::: StockCheckRespository :::: method :::: stockCheck");
        return inventoryStockCheck.stockCheck(inventoryCheck);
    }
}
