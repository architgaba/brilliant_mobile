package com.sage.api.service.salesorder.salesordertransaction;

import com.google.gson.Gson;
import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.utils.*;
import com.sage.api.domain.template.MailSender;
import com.sage.api.repository.purchaseorder.transactions.PurchaseOrderTransactionRepository;
import com.sage.api.repository.salesorder.salesordertransaction.SalesOrderTransactionRepository;
import com.sage.api.repository.template.TemplateRepository;
import com.sage.api.service.template.TemplateService;
import com.sage.api.utils.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SalesOrderTransactionServiceImpl implements SalesOrderTransactionService {
    private static final Logger log = LogManager.getLogger(SalesOrderTransactionServiceImpl.class);
    @Autowired
    private SalesOrderTransactionRepository salesOrderTransactionRespository;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PDFConverter pdfConverter;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private PurchaseOrderTransactionRepository purchaseOrderTransactionRepository;

    /**
     * FETCHING WAREHOUSE LIST
     **/
    @Override
    public ResponseEntity fetchWarehouse() {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchWarehouse");
        return salesOrderTransactionRespository.fetchWarehouse();
    }

    /**
     * FETCHING LINES OF A DOCUMENT
     **/
    @Override
    public ResponseEntity<?> fetchingLinesDetails(String docId, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingLinesDetails");
        String apiUrl = FreedomServicesConstants.baseUrl + "/SalesOrderLoadByID?ID=" + docId;
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        HttpEntity<?> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
            String obj = (String) response.getBody();
            int count1, count2, count3 = 0;
            String lineId = "", test1 = "", financial = "";
            if (obj != null) {
                String OrderNo = obj.substring(obj.lastIndexOf("OrderNo") + 10, obj.lastIndexOf("OrderPriority") - 3);
                String dueDate = obj.substring(obj.indexOf("DueDate") + 10, obj.indexOf("ExternalOrderNo") - 3);
                String docHash = obj.substring(obj.indexOf("["), obj.indexOf("]"));
                int startPoint = obj.lastIndexOf("Lines") - 1;
                int endPoint = obj.lastIndexOf("OrderDate") - 2;
                String output2 = obj.substring(startPoint, endPoint);
                if (output2.length() > 30) {
                    for (int i = 0; i < output2.length(); ) {
                        count1 = output2.indexOf("LineID", output2.indexOf("LineID") + 10 + count3);
                        count2 = output2.indexOf("ProjectCode", i);
                        count3 = output2.indexOf("}", output2.indexOf("}") + 1 + count3);
                        lineId = output2.substring(count1 + 8, count2 - 2);
                        LineNotes qty = salesOrderTransactionRespository.getQtyProcessed(lineId);
                        test1 = test1 + "{\"lineId\":" + lineId + "," + "\"qtyProcessed\":" + qty.getQty() + "," + "\"lineNote\":" + "\"" + qty.getLineNotes() + "\"" + "," + "\"fUnitPriceIncl\":" + qty.getFUnitPriceIncl() + ",\"taxRate\":" + qty.getTaxRate() + "},";
                        i = count3 + 2;
                    }
                } else {
                    output2 = "\"Lines\":[]";
                    test1 = " ";
                }
                count3 = 0;
                startPoint = obj.indexOf("FinancialLines");
                endPoint = obj.indexOf("InvoiceDate");
                String financialLines = obj.substring(startPoint - 1, endPoint - 2);
                if (financialLines.length() > 30) {
                    for (int i = 0; i < financialLines.length(); ) {
                        count1 = financialLines.indexOf("LineID", financialLines.indexOf("LineID") + 10 + count3);
                        count2 = financialLines.indexOf("ProjectCode", i);
                        count3 = financialLines.indexOf("}", financialLines.indexOf("}") + 1 + count3);
                        lineId = financialLines.substring(count1 + 8, count2 - 2);
                        LineNotes qty = salesOrderTransactionRespository.getQtyProcessed(lineId);

                        financial = financial + "{\"lineId\":" + lineId + "," + "\"qtyProcessed\":" + qty.getQty() + "," + "\"lineNote\":" + "\"" + qty.getLineNotes() + "\"" + "," + "\"fUnitPriceIncl\":" + qty.getFUnitPriceIncl() + "," + "\"taxRate\":" + qty.getTaxRate() + "},";
                        i = count3 + 2;
                    }
                } else {
                    financialLines = "\"FinancialLines\":[]";
                    financial = " ";
                }
                String signature = salesOrderTransactionRespository.fetchingsignaturedetails(docId);
                String lineIdOutput = "{" + output2 + "," + "\"qtyProcessed\":[" + test1.substring(0, test1.length() - 1) + "]," + "\"signature\":" + "\"" + signature + "\"," + "\"documentHash\":" + "" + docHash + "]," + "\"dueDate\":" + "\"" + dueDate + "\"," + "\"OrderNo\":" + "\"" + OrderNo + "\"," + financialLines + ",\"financialLinesQtyProcessed\":[" + financial.substring(0, financial.length() - 1) + "]}";
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingLinesDetails");
                return new ResponseEntity<>(lineIdOutput, responseHeaders, HttpStatus.OK);
            } else {
                log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingLinesDetails");
                return ResponseDomain.successResponse("No data Available", true);
            }
        } catch (Exception e) {
            log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingLinesDetails :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Server Error", false);
        }
    }

    /**
     * FETCHING CUSTOMER DETAILS
     **/
    @Override
    public ResponseEntity<?> fetchingCustomerDetails(String name, String docId) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingCustomerDetails");
        return salesOrderTransactionRespository.fetchingCustomerDetails(name, docId);
    }

    /**
     * FETCHING ITEM CODE AND DESCRIPTION LIST OF STOCK
     **/
    @Override
    public ResponseEntity<?> fetchingItemCode() {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingItemCode");
        return salesOrderTransactionRespository.fetchingItemCode();
    }

    /**
     * FETCHING ALL CUSTOMERS
     **/
    @Override
    public ResponseEntity<?> fetchCustomers() {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchCustomers");
        return salesOrderTransactionRespository.fetchingCustomerNames();
    }

    /**
     * FETCHING DOCUMENT DETAILS
     **/
    @Override
    public ResponseEntity<?> fetchDocumentDetails(String name, String docId) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchDocumentDetails");
        return salesOrderTransactionRespository.fetchingDocumentDetails(name, docId);
    }

    /**
     * FETCHING DOCUMENT MORE DETAILS
     **/
    @Override
    public ResponseEntity<?> fetchMoreDocumentDetails(String docId) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchMoreDocumentDetails");
        return salesOrderTransactionRespository.fetchingDocumentMoreDetails(docId);
    }

    /**
     * FETCHING STOCK DETAILS
     **/
    @Override
    public ResponseEntity<?> fetchingStockData(StockData data) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: fetchingStockData");
        return salesOrderTransactionRespository.fetchingStockDetails(data);
    }

    /**
     * CREATE/PLACE NEW SALES ORDER
     **/
    @Override
    public ResponseEntity<?> createPlaceSalesOrder(WrapperQuotes wrapperQuotes, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/SalesOrderPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String accountCode = wrapperQuotes.getQuotes().getQuote().getCustomerAccountCode();
        Quote salesOrder = wrapperQuotes.getQuotes();
        Integer docId = wrapperQuotes.getQuotes().getQuote().getDocumentID();
        HttpEntity<Object> entity = new HttpEntity<>(salesOrder, headers);
        try {
            synchronized (this) {
                if (docId != 0) {
                    salesOrderTransactionRespository.updateDocState(docId);
                    salesOrder.getQuote().setDocumentID(0);
                }
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error Occurred while Creating/Placing Sales Order through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String orderNumber = salesOrderTransactionRespository.orderNumber(accountCode);
                        Integer documentId = salesOrderTransactionRespository.fetchingDocumentId(accountCode);
                        salesOrderTransactionRespository.updateAgentIDAgainstOrder(documentId, jwtTokenUtil.getAgentIdFromToken(authKey));
                        salesOrderTransactionRespository.saveSignature(wrapperQuotes.getSignature(), documentId);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        Map<String, String> stNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("S"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> stLineNotes = new TreeMap<String, String>(stNotes);
                        Map<String, String> glNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("G"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> glLineNotes = new TreeMap<String, String>(glNotes);
                        String[] stLineNotesArray = Arrays.copyOf(stLineNotes.values().toArray(), stLineNotes.values().toArray().length, String[].class);
                        String[] glLineNotesArray = Arrays.copyOf(glLineNotes.values().toArray(), glLineNotes.values().toArray().length, String[].class);
                        if (stLineNotesArray != null && stLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, stLineNotesArray);
                        if (glLineNotesArray != null && glLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);

                      /*  String[] lineNotesArray = wrapperQuotes.getLinenotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);*/
                        log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
                        return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "" + orderNumber, true), HttpStatus.OK);

                    }
                } else {
                    log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
                    return ResponseDomain.internalServerError("No Order Created Through Freedom Service", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createPlaceSalesOrder");
            return ResponseDomain.internalServerError("Server Error", false);
        }
    }

    /**
     *
     * UPDATING SALES ORDER
     *
     * */
    @Override
    public ResponseEntity<?> updateExistingOrder(WrapperSalesorder wrapperSalesorder, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/SalesOrderExistingPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String accountCode = wrapperSalesorder.getOrders().getOrder().getCustomerAccountCode();
        Integer id = wrapperSalesorder.getOrders().getOrder().getDocumentID();
        String documentId = "" + id;
        SalesOrderRO salesOrderRO = wrapperSalesorder.getOrders();
        HttpEntity<Object> entity = new HttpEntity<>(salesOrderRO, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error Occurred Updating Order/Quote through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        salesOrderTransactionRespository.updateAgentIDAgainstOrder(id, jwtTokenUtil.getAgentIdFromToken(authKey));
                        String Signature = wrapperSalesorder.getSignature();
                        salesOrderTransactionRespository.saveSignature(Signature, id);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(id);
                       /* String[] lineNotesArray = wrapperSalesorder.getLinenotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);*/

                        Map<String, String> stNotes = wrapperSalesorder.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("S"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> stLineNotes = new TreeMap<String, String>(stNotes);
                        Map<String, String> glNotes = wrapperSalesorder.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("G"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> glLineNotes = new TreeMap<String, String>(glNotes);
                        String[] stLineNotesArray = Arrays.copyOf(stLineNotes.values().toArray(), stLineNotes.values().toArray().length, String[].class);
                        String[] glLineNotesArray = Arrays.copyOf(glLineNotes.values().toArray(), glLineNotes.values().toArray().length, String[].class);
                        if (stLineNotesArray != null && stLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, stLineNotesArray);
                        if (glLineNotesArray != null && glLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);

                        String docState = salesOrderTransactionRespository.docState(documentId);
                        if (docState.equals("2")) {
                            String orderNumber = salesOrderTransactionRespository.quoteNumber(documentId, accountCode);
                            return new ResponseEntity<>(new ResponseDomain(true, "" + orderNumber), HttpStatus.OK);
                        } else {
                            String invoiceNumber = salesOrderTransactionRespository.updatedOrderNum(documentId, accountCode);
                            return new ResponseEntity<>(new ResponseDomain(true, "" + invoiceNumber), HttpStatus.OK);
                        }
                    }
                } else {
                    log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder");
                    return ResponseDomain.internalServerError("Order Not Updated", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExistingOrder :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     *
     * INVOICING EXISTING SALES ORDER/QUOTE
     *
     **/
    @Override
    public ResponseEntity<?> updateExitingSalesOrder(WrapperOrder wrapperOrder, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/SalesOrderExistingProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String accountCode = wrapperOrder.getOrders().getOrder().getCustomerAccountCode();
        Integer documentId = wrapperOrder.getOrders().getOrder().getDocumentID();
        OrderRO orderRO = wrapperOrder.getOrders();
        HttpEntity<Object> entity = new HttpEntity<>(orderRO, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.error("Error Occurred Invoicing Sales Order/Quote through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        salesOrderTransactionRespository.updateAgentIDAgainstOrder(documentId, jwtTokenUtil.getAgentIdFromToken(authKey));
                        String invoiceNumber = salesOrderTransactionRespository.fetchingInvoiceNum(documentId, accountCode);
                        salesOrderTransactionRespository.saveSignature(wrapperOrder.getSignature(), documentId);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        Map<String, String> stNotes = wrapperOrder.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("S"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> stLineNotes = new TreeMap<String, String>(stNotes);
                        Map<String, String> glNotes = wrapperOrder.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("G"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> glLineNotes = new TreeMap<String, String>(glNotes);
                        String[] stLineNotesArray = Arrays.copyOf(stLineNotes.values().toArray(), stLineNotes.values().toArray().length, String[].class);
                        String[] glLineNotesArray = Arrays.copyOf(glLineNotes.values().toArray(), glLineNotes.values().toArray().length, String[].class);
                        if (stLineNotesArray != null && stLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, stLineNotesArray);
                        if (glLineNotesArray != null && glLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);
                    /*    String[] lineNotesArray = wrapperOrder.getLinenotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);*/
                        return ResponseDomain.successResponse("" + invoiceNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder");
                    return ResponseDomain.internalServerError("Server Error in Updating Order", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateExitingSalesOrder :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     *
     * CREATING INVOICE
     *
     **/
    @Override
    public ResponseEntity<?> createInvoice(WrapperQuotes wrapperQuotes, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
        String apiUrl = FreedomServicesConstants.baseUrl + "/SalesOrderProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String accountCode = wrapperQuotes.getQuotes().getQuote().getCustomerAccountCode();
        Quote salesOrder = wrapperQuotes.getQuotes();
        List<Lines> lines = salesOrder.getQuote().getLines();
        String backOrderCheck = "";
        for (Lines entity : lines) {
            if (entity.getQuantity() > entity.getToProcess())
                backOrderCheck = "Generate";
        }
        HttpEntity<Object> entity = new HttpEntity<>(salesOrder, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.error("Error Occurred Creating Invoice through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String invoiceNumber = salesOrderTransactionRespository.invoiceNumber(accountCode);
                        Integer documentId = salesOrderTransactionRespository.fetchingDocumentId(accountCode);
                        salesOrderTransactionRespository.updateAgentIDAgainstOrder(documentId, jwtTokenUtil.getAgentIdFromToken(authKey));
                        String signature = wrapperQuotes.getSignature();
                        salesOrderTransactionRespository.saveSignature(signature, documentId);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        Map<String, String> stNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("S"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> stLineNotes = new TreeMap<String, String>(stNotes);
                        Map<String, String> glNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("G"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> glLineNotes = new TreeMap<String, String>(glNotes);
                        String[] stLineNotesArray = Arrays.copyOf(stLineNotes.values().toArray(), stLineNotes.values().toArray().length, String[].class);
                        String[] glLineNotesArray = Arrays.copyOf(glLineNotes.values().toArray(), glLineNotes.values().toArray().length, String[].class);
                        if (stLineNotesArray != null && stLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, stLineNotesArray);
                        if (glLineNotesArray != null && glLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);
                      /*  String[] lineNotesArray = wrapperQuotes.getLinenotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);*/
                        if (backOrderCheck.equalsIgnoreCase("Generate")) {
                            Integer backOrderID = documentId - 1;
                            salesOrderTransactionRespository.saveSignature(signature, backOrderID);
                            salesOrderTransactionRespository.updateAgentIDAgainstOrder(backOrderID, jwtTokenUtil.getAgentIdFromToken(authKey));
                            List<Integer> lineListBack = salesOrderTransactionRespository.fetchLinesDetails(backOrderID);
                            if (stLineNotesArray != null && stLineNotesArray.length > 0)
                                salesOrderTransactionRespository.lineNotesUpdate(lineListBack, stLineNotesArray);
                            if (glLineNotesArray != null && glLineNotesArray.length > 0)
                                salesOrderTransactionRespository.lineNotesUpdate(lineListBack.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);
                        }
                        log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
                        return ResponseDomain.successResponse("" + invoiceNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice");
                    return ResponseDomain.internalServerError("Sever Error in Creating Invoice", true);
                }
            }
        } catch (Exception e) {
            log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createInvoice :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * CREATING SALES ORDER QUOTATION
     **/
    @Override
    public ResponseEntity<?> createSalesOrderQuotes(WrapperQuotes wrapperQuotes, String authKey) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
        String apiUrl = FreedomServicesConstants.baseUrl + "/QuotationPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        log.info(jwtTokenUtil.getPasswordFromToken(authKey));
        String accountCode = wrapperQuotes.getQuotes().getQuote().getCustomerAccountCode();
        Quote salesOrder = wrapperQuotes.getQuotes();
        HttpEntity<Object> entity = new HttpEntity<>(salesOrder, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.error("Error Occurred Creating Quote through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String orderNumber = salesOrderTransactionRespository.fetchingOrderNumInvoice(accountCode);
                        Integer documentId = salesOrderTransactionRespository.fetchingDocumentId(accountCode);
                        salesOrderTransactionRespository.updateAgentIDAgainstOrder(documentId, jwtTokenUtil.getAgentIdFromToken(authKey));
                        String signature = wrapperQuotes.getSignature();
                        salesOrderTransactionRespository.saveSignature(signature, documentId);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        Map<String, String> stNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("S"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> stLineNotes = new TreeMap<String, String>(stNotes);
                        Map<String, String> glNotes = wrapperQuotes.getLinenotes().entrySet()
                                .stream()
                                .filter(map -> map.getKey().startsWith("G"))
                                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
                        Map<String, String> glLineNotes = new TreeMap<String, String>(glNotes);
                        String[] stLineNotesArray = Arrays.copyOf(stLineNotes.values().toArray(), stLineNotes.values().toArray().length, String[].class);
                        String[] glLineNotesArray = Arrays.copyOf(glLineNotes.values().toArray(), glLineNotes.values().toArray().length, String[].class);
                        if (stLineNotesArray != null && stLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, stLineNotesArray);
                        if (glLineNotesArray != null && glLineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList.subList(stLineNotesArray.length, lineList.size()), glLineNotesArray);
                      /*  String[] lineNotesArray = wrapperQuotes.getLinenotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);*/
                        log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
                        return ResponseDomain.successResponse("" + orderNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes");
                    return new ResponseEntity<>(new ResponseDomain(true, "Server Error in Creating Quote"), HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: createSalesOrderQuotes :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     * DELETING LINES
     **/
    @Override
    public ResponseEntity<?> deleteLine(String lineId) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: deleteLine");
        return salesOrderTransactionRespository.deleteLine(lineId);
    }


    /**
     * DOWNLOADING / VIEWING TEMPLATE
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(String reference, String action) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: templateFunctionality");
        TemplateFunctionality templateFunctionality = new TemplateFunctionality();
        templateFunctionality.setAction(action);
        templateFunctionality.setReference(reference);
        return salesOrderTransactionRespository.templateFunctionality(templateFunctionality);
    }

    /**
     * MAILING TEMPLATE
     **/
    @Override
    public ResponseEntity<?> templateMailer(MailSender sender, String reference) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: templateMailer");
        return salesOrderTransactionRespository.mailSender(sender, reference);
    }

    /**
     * FETCHING RECEIPTS DEFAULT DATA
     **/
    @Override
    public ResponseEntity<?> receiptsData(String invoiceNum) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: receiptsData");
        return salesOrderTransactionRespository.receiptsData(invoiceNum);
    }

    /**
     * FETCHING FINANCIAL LINES DROPDOWN(GL)
     **/
    @Override
    public ResponseEntity<?> salesOrderGLDropdown() {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: salesOrderGLDropdown");
        return salesOrderTransactionRespository.glAccountDropDown();
    }

    /**
     * SAVING/UPDATING CLIENT DETAILS AGAINST AN ORDER
     **/
    @Override
    public ResponseEntity<?> updateCustomFields(String orderNum, String docId, AccountDetails details) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: updateCustomFields");
        return salesOrderTransactionRespository.updateCustomFields(orderNum, docId, details);
    }
}
