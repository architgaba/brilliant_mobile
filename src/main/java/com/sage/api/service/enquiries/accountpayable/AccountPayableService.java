package com.sage.api.service.enquiries.accountpayable;

import com.sage.api.domain.enquiries.Accounts;
import org.springframework.http.ResponseEntity;

public interface AccountPayableService {

    ResponseEntity<?> fetchingAllAccounts();

    ResponseEntity<?> fetchingAccountData( Accounts accounts);
}
