package com.sage.api.service.enquiries.inventory;

import com.sage.api.domain.enquiries.Inventory;
import com.sage.api.repository.enquiries.inventory.InventoryRespository;
import com.sage.api.service.enquiries.accountreceivable.AccountReceivableServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class InventoryServiceImpl implements InventoryService {
    private static final Logger log= LogManager.getLogger(AccountReceivableServiceImpl.class);
    @Autowired
    private  InventoryRespository  inventory;

    /**
     *
     * FETCHING ALL INVENTORY ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllInventoryAccounts() {
        log.info("Entering Service Class :::: InventoryServiceImpl :::: method :::: fetchingAllInventoryAccounts");
        return inventory.fetchingInventoryAccounts();
    }

    /**
     *
     * FETCHING INVENTORY DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?>  inventoryData(Inventory inventoryData) {
        log.info("Entering Service Class :::: InventoryServiceImpl :::: method :::: inventoryData");
        return inventory.inventoryData(inventoryData);
    }
}
