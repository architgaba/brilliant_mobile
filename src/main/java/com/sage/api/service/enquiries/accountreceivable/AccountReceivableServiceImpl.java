package com.sage.api.service.enquiries.accountreceivable;

import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.repository.enquiries.accountreceivable.AccountReceivableRepo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountReceivableServiceImpl implements AccountReceivableService {

    private static final Logger log= LogManager.getLogger(AccountReceivableServiceImpl.class);
    @Autowired
    private AccountReceivableRepo accountReceivableRepo;

    /**
     *
     * FETCHING ALL RECEIVABLE(CLIENT) ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllAccounts() {
        log.info("Entering Service Class :::: AccountReceivableServiceImpl :::: method :::: fetchingAllAccounts");
        return accountReceivableRepo.fetchingAllAccounts();
    }

    /**
     *
     * FETCHING ACCOUNT(CLIENT) RECEIVABLE DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> accountReceivableData(Accounts accounts) {
        log.info("Entering Service Class :::: AccountReceivableServiceImpl :::: method :::: accountReceivableData");
        return accountReceivableRepo.accountReceivableData(accounts);
    }
}
