package com.sage.api.service.purchaseorder.transactions;

import com.google.gson.Gson;
import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.ExistingPurchaseOrderRO;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.PurchaseOrderRO;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperExistingPuchaseOrder;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperPurchaseOrder;
import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.salesorder.salesordertransaction.TemplateFunctionality;
import com.sage.api.domain.template.MailSender;
import com.sage.api.repository.purchaseorder.transactions.PurchaseOrderTransactionRepository;
import com.sage.api.repository.salesorder.salesordertransaction.SalesOrderTransactionRepository;
import com.sage.api.utils.Error;
import com.sage.api.utils.FreedomServicesConstants;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.ValidationErrors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;

@Service
public class PurchaseOrderTransactionServiceImpl implements PurchaseOrderTransactionService {

    private static final Logger log = LogManager.getLogger(PurchaseOrderTransactionServiceImpl.class);
    @Autowired
    private PurchaseOrderTransactionRepository purchaseOrderListRepository;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private SalesOrderTransactionRepository salesOrderTransactionRespository;

    /**
     *
     * ORDER/PLACE PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> placePurchaseOrder(String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in Creating Purchase Order through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                            return ResponseDomain.badRequest(""+Message,false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                                Message= Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                            return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.",false);
                    } else {
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                        salesOrderTransactionRespository.saveSignature( wrapperPurchaseOrderRO.getSignature(), documentId);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(documentId.toString(), jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                        return ResponseDomain.successResponse(""+purchaseOrderNumber,true);
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                    return ResponseDomain.internalServerError("Order Not Placed by Freedom Services",false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error / "+e,false);
        }
    }

    /**
     *
     * UPDATE EXISTING PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> placeExistingPurchaseOrder(String authKey, WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in Updating existing Purchase Order through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        Integer documentIdPurchase = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getDocumentID();
                        String docId = "" + documentIdPurchase;
                        salesOrderTransactionRespository.saveSignature(WrapperExistingPuchaseOrder.getSignature(), documentIdPurchase);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(docId, jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentIdPurchase);
                        String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                             salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        String docState = purchaseOrderListRepository.docState(docId);
                        if (docState.equals("2")) {
                            String orderNumber = purchaseOrderListRepository.purchaseQuoteNumber(docId, supplierCode);
                            return ResponseDomain.successResponse("" + orderNumber, true);
                        } else {
                            String purchaseOrderNumber = purchaseOrderListRepository.purchaseQuoteNumber(docId, supplierCode);
                            return ResponseDomain.successResponse("" + purchaseOrderNumber, true);
                        }
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                    return ResponseDomain.internalServerError("Order Not Updated Through freedom Service", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * CREATING NEW INVOICE
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderExistingProcessInvoice(String authKey, WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.error("Error occurred in creating New Invoice through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String grvNumber = purchaseOrderListRepository.grvInvoiceNumber(supplierCode);
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                        purchaseOrderListRepository.saveSignature(wrapperPurchaseOrderRO.getSignature(),purchaseOrderNumber);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(documentId.toString(), jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                             salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        return ResponseDomain.successResponse("" + grvNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                    return ResponseDomain.internalServerError("No Invoice Generated through Freedom Service", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * EXISTING PURCHASE ORDER INTO INVOICE
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderProcessInvoice(String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        Integer lineNotesUpdate;
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in Invoicing existing Purchase Order through Freedom Services");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        Integer documentId = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getDocumentID();
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        String grvNumber = purchaseOrderListRepository.grvInvoiceNumber(supplierCode);
                        purchaseOrderListRepository.saveSignature(WrapperExistingPuchaseOrder.getSignature(),purchaseOrderNumber);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(documentId.toString(), jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                        return ResponseDomain.successResponse("" + grvNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                    return ResponseDomain.successResponse("No Invoice Processed", true);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * NEW RECEIVE STOCK
     *
     *
     **/
    @Override
    public ResponseEntity<?> existingReceiveStock(String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderReceiveStock";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        Integer lineNotesUpdate;
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in creating new receive stock through Freedom Service");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String grvNumber = purchaseOrderListRepository.grvInvoiceNumber(supplierCode);
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        purchaseOrderListRepository.saveSignature(wrapperPurchaseOrderRO.getSignature(),purchaseOrderNumber);
                        Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(documentId.toString(), jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        return ResponseDomain.successResponse("" + grvNumber, true);
                    }
                } else {
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                    return ResponseDomain.internalServerError("No Receive Stock Created", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * EXISTING RECEIVE STOCK IN PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderReceiveStock(String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingReceiveStock";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        log.info(entity.getBody());
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in Existing Receive Stock through Freedom Service");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                            ;
                            return ResponseDomain.badRequest("" + Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors()) {
                                Message = Arrays.toString(err.getErrors());
                            }
                            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                            return ResponseDomain.badRequest("" + Message.substring(1, Message.length() - 1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                        String grvNumber = purchaseOrderListRepository.grvNumber(documentId, supplierCode);
                        String signature = WrapperExistingPuchaseOrder.getSignature();
                        salesOrderTransactionRespository.saveSignature(signature, documentId);
                        purchaseOrderListRepository.updateAgentIdAgainstOrder(documentId.toString(), jwtTokenUtil.getAgentIdFromToken(authKey));
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        return ResponseDomain.successResponse("" + grvNumber, true);
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                    return ResponseDomain.internalServerError("Server Error", false);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING GL DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderGLDropdown() {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderGLDropdown");
        return purchaseOrderListRepository.glAccountDropDown();
    }

    /**
     *
     * TEMPLATE FUNCTIONALITY(EMAIL)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateMailer(MailSender sender, String reference) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: templateMailer");
        return purchaseOrderListRepository.mailSender(sender, reference);
    }

    /**
     *
     * TEMPLATE FUNCTIONALITY(DOWNLOAD/PREVIEW)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(String reference, String action) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: templateFunctionality");
        TemplateFunctionality templateFunctionality = new TemplateFunctionality();
        templateFunctionality.setAction(action);
        templateFunctionality.setReference(reference);
        return purchaseOrderListRepository.templateFunctionality(templateFunctionality);
    }

    /**
     *
     * FETCHING STOCK DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderSTKDropdown() {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderSTKDropdown");
        return purchaseOrderListRepository.STKDropDown();
    }

    /**
     *
     * FETCHING SUPPLIER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingSupplierDetails(String name, String docId) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: fetchingSupplierDetails");
        return purchaseOrderListRepository.supplierDetails(name, docId);
    }

    /**
     *
     * SAVING/UPDATING SUPPLIER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> updateCustomFields(String orderNum, String docId, AccountDetails details) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: updateCustomFields");
        return purchaseOrderListRepository.updateCustomFields(orderNum,docId,details);
    }
}