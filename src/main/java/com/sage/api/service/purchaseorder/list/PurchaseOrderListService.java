package com.sage.api.service.purchaseorder.list;

import com.sage.api.domain.purchaseorder.list.PurchaseOrderList;
import org.springframework.http.ResponseEntity;

public interface PurchaseOrderListService {

    ResponseEntity<?> fetchingSuppliers();

    ResponseEntity<?> fetchingListData(PurchaseOrderList salesOrderList);
}
