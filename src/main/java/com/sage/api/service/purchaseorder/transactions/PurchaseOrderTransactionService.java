package com.sage.api.service.purchaseorder.transactions;

import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperExistingPuchaseOrder;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperPurchaseOrder;
import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.template.MailSender;
import org.springframework.http.ResponseEntity;

public interface PurchaseOrderTransactionService {

    ResponseEntity<?> placePurchaseOrder (String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO);

    ResponseEntity<?> placeExistingPurchaseOrder (String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder);

    ResponseEntity<?> purchaseOrderExistingProcessInvoice (String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO);

    ResponseEntity<?> purchaseOrderProcessInvoice (String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder);

    ResponseEntity<?> existingReceiveStock (String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO);

    ResponseEntity<?> purchaseOrderReceiveStock (String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder);

    ResponseEntity<?> purchaseOrderGLDropdown();

    ResponseEntity<?> purchaseOrderSTKDropdown();

    ResponseEntity<?> templateFunctionality(String reference, String action);

    ResponseEntity<?> templateMailer(MailSender sender, String reference);

    ResponseEntity<?> fetchingSupplierDetails(String name, String docId);

    ResponseEntity<?> updateCustomFields(String orderNum,String docId,AccountDetails details);
}
