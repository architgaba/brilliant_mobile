package com.sage.api.service.defaults;

import com.sage.api.domain.defaults.AgentDefaults;
import com.sage.api.repository.defaults.DefaultsRepository;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DefaultsServiceImpl implements DefaultsService{
    private static final Logger log = LogManager.getLogger(DefaultsServiceImpl.class);
    @Autowired
    private DefaultsRepository defaultsRepository;

    /*
     *
     * FETCHING DEFAULTS DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> defaultData() {
        log.info("Entering Service Class :::: DefaultServiceImpl :::: method :::: defaultData");
        return defaultsRepository.defaultsData();
    }

    /*
     *
     * SAVING/UPDATING AGENTS DASHBOARD DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> saveAgentDefaults(AgentDefaults defaults) {
        log.info("Entering Service Class :::: DefaultServiceImpl :::: method :::: saveAgentDefaults");
        if (defaults!=null)
            return defaultsRepository.savingDefaultsInfo(defaults);
        log.info("Exiting Service Class :::: DefaultServiceImpl :::: method :::: saveAgentDefaults");
        return ResponseDomain.internalServerError("Agent Defaults Missing",false);
    }

    /*
     *
     * RETRIEVING AGENTS DEFAULTS
     *
     *
     */
    @Override
    public ResponseEntity<?> getAgentDefaults(String agentName) {
        log.info("Entering Service Class :::: DefaultServiceImpl :::: method :::: getAgentDefaults");
        if (agentName != null)
            return defaultsRepository.fetchingAgentDefaults(agentName);
        log.info("Exiting Service Class :::: DefaultServiceImpl :::: method :::: getAgentDefaults");
        return ResponseDomain.internalServerError("Internal Server Error", false);
    }
}
