package com.sage.api.service.defaults;

import com.sage.api.domain.defaults.AgentDefaults;
import org.springframework.http.ResponseEntity;

public interface DefaultsService  {

     ResponseEntity<?> defaultData();

     ResponseEntity<?> saveAgentDefaults(AgentDefaults defaults);

     ResponseEntity<?> getAgentDefaults(String agentName);
}

