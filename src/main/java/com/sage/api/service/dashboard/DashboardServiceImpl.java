package com.sage.api.service.dashboard;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.repository.dashboard.DashboardRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService {
    private static final Logger log = LogManager.getLogger(DashboardServiceImpl.class);
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private DashboardRepository dashboardRepository;

    /*
     *
     *FETCHING DASHBOARD DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> data(String authKey) {
        log.info("Entering Service Class :::: DashboardServiceImpl :::: method :::: data");
        return dashboardRepository.data(jwtTokenUtil.getAgentIdFromToken(authKey),jwtTokenUtil.getUsernameFromToken(authKey));
    }

    /*
     *
     * LOGGING OUT AGENT
     *
     *
     */
    @Override
    public ResponseEntity<?> agentLogOut(String authKey) {
        log.info("Entering Service Class :::: DashboardServiceImpl :::: method :::: agentLogOut");
        return dashboardRepository.loggingOut(jwtTokenUtil.getAgentIdFromToken(authKey),jwtTokenUtil.getDeviceTypeFromToken(authKey));
    }
}
