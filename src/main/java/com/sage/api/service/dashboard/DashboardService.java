package com.sage.api.service.dashboard;

import org.springframework.http.ResponseEntity;

public interface DashboardService {

    ResponseEntity<?> data(String key);

    ResponseEntity<?> agentLogOut(String token);
}
