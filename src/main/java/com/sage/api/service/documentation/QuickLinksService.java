package com.sage.api.service.documentation;

import com.sage.api.domain.documentation.ParameterResultVO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

public interface QuickLinksService {

    public ResponseEntity<?> getEndPointsList();

    public ResponseEntity<?> insertEndPoint(String endpoint, String description, String category);

    public ResponseEntity<?> getEndPointById(String id);

    public ResponseEntity<?> insertParametersById(ParameterResultVO parameterResultVO);
}
