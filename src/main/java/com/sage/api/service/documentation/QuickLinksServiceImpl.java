package com.sage.api.service.documentation;

import com.sage.api.domain.documentation.*;
import com.sage.api.repository.documentation.ParametersRepo;
import com.sage.api.repository.documentation.QuickLinksRepo;
import com.sage.api.repository.documentation.ResultsRepo;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuickLinksServiceImpl implements QuickLinksService {


    @Autowired
    private QuickLinksRepo quickLinksRepo;
    @Autowired
    private ParametersRepo parametersRepo;
    @Autowired
    private ResultsRepo resultsRepo;

    private static final Logger log = LogManager.getLogger(QuickLinksServiceImpl.class);

    @Override
    public ResponseEntity<?> getEndPointsList() {
        log.info("Enter into QuickLinksServiceImpl class inside method getEndPointsList");
        JSONObject responseQuickLinkObj = new JSONObject();
        List<String> categoryList = quickLinksRepo.findCategory();
        log.info(categoryList);
        if (!categoryList.isEmpty()) {
            categoryList.forEach(category -> {
                List<QuickLinks> quickLinksList = quickLinksRepo.findByCategory(category);
                log.info(quickLinksList);
                List list = new ArrayList();
                quickLinksList.forEach(element -> {
                    JSONObject quickLinkObj = new JSONObject();
                    quickLinkObj.put("endpoint", element.getEndPoint());
                    quickLinkObj.put("description", element.getDescription());
                    quickLinkObj.put("id", element.getLinkId());
                    list.add(quickLinkObj);
                });
                responseQuickLinkObj.put(category, list);

            });
            log.info(responseQuickLinkObj);
            return new ResponseEntity<>(responseQuickLinkObj.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<>("No Data Exists", HttpStatus.BAD_REQUEST);
    }


    @Override
    public ResponseEntity<?> insertEndPoint(String endpoint, String description, String category) {
        log.info("Enter into QuickLinksServiceImpl class inside method insertEndPoint");
        QuickLinks quickLinks = new QuickLinks();
        quickLinks.setCategory(category);
        quickLinks.setDescription(description);
        quickLinks.setEndPoint(endpoint);
        quickLinksRepo.save(quickLinks);
        return new ResponseEntity<>("endpoint inserted successfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getEndPointById(String id) {
        log.info("Enter into QuickLinksServiceImpl class inside method getEndPointById");
        QuickLinks selectedQuickLink = quickLinksRepo.findByLinkId(Long.parseLong(id));
        Parameters parametersObj = parametersRepo.findByQuickLinksParam_LinkId(Long.parseLong(id));
        Result resultObj = resultsRepo.findByQuickLinksRes_LinkId(Long.parseLong(id));
        if ((selectedQuickLink != null) && (parametersObj != null) && (resultObj != null)) {
            log.info(parametersObj);
            log.info(resultObj);
            JSONObject jsonObject = new JSONObject();
            List<ResponsesTable> resultTableList = resultObj.getResponseTable();
            ArrayList resultList=new ArrayList();

            resultTableList.forEach(list->{
                JSONObject result=new JSONObject();
                result.put("name",list.getName());
                result.put("type",list.getType());
                result.put("description",list.getDescription());
                resultList.add(result);
            });

            List<ResponseTable> parameterTableList = parametersObj.getResponseTable();
            ArrayList parameterList=new ArrayList();
            parameterTableList.forEach(list->{
                JSONObject parameter=new JSONObject();
                parameter.put("name",list.getName());
                parameter.put("type",list.getType());
                parameter.put("description",list.getDescription());
                parameterList.add(parameter);
            });

            jsonObject.put("parameter", parameterList);
            jsonObject.put("result", resultList);
            jsonObject.put("url", selectedQuickLink.getUrl());
            jsonObject.put("HTTP_Method", selectedQuickLink.getMethods());
            jsonObject.put("description", selectedQuickLink.getDescription());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<>("invalid endpointId or childs not exist", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<?> insertParametersById(ParameterResultVO parameterResultVO) {
        log.info("Enter into QuickLinksServiceImpl class inside method insertparametersbyid");
        Parameters parameters = null;
        Result result = null;
        QuickLinks quickLinksObj = quickLinksRepo.findByLinkId(parameterResultVO.getQuickLinkId());
        if (quickLinksObj != null) {
            result= new Result();
            parameters= new Parameters();
            quickLinksObj.setUrl(parameterResultVO.getUrl());
            quickLinksObj.setMethods(parameterResultVO.getMethods());
            quickLinksRepo.save(quickLinksObj);
//            Parametersvo parametersVO = parameterResultVO.getParametersvo();
//            parametersVO.setResponseTable(parameterResultVO.getParametersvo().getResponseTable());
            ArrayList<ResponseTable> parameterTable=new ArrayList<>();
            parameterResultVO.getParametersvo().getResponseTable().forEach(list->{
                parameterTable.add(new ResponseTable(list.getName(),list.getType(),list.getDescription()));
            });
            log.info(parameterTable);
            parameters.setResponseTable(parameterTable);
            parameters.setQuickLinksParam(quickLinksObj);
            parametersRepo.save(parameters);
//            ResultVO resultVO = parameterResultVO.getResultVO();
//            resultVO.setResponseTable(parameterResultVO.getResultVO().getResponseTable());
            ArrayList<ResponsesTable> resultTable=new ArrayList<>();
            parameterResultVO.getResultVO().getResponseTable().forEach(list->{
                resultTable.add(new ResponsesTable(list.getName(),list.getType(),list.getDescription()));
            });
            log.info(resultTable);
            result.setResponseTable(resultTable);
            result.setQuickLinksRes(quickLinksObj);
            resultsRepo.save(result);
            return new ResponseEntity<>("parameters inserted successfully", HttpStatus.OK);
        }


        return new ResponseEntity<>("invalid linkId", HttpStatus.BAD_REQUEST);
    }
}
