package com.sage.api.repository.defaults;

import com.sage.api.domain.defaults.AgentDefaults;
import org.springframework.http.ResponseEntity;

public interface DefaultsRepository {

     ResponseEntity<?> defaultsData();

     ResponseEntity<?> savingDefaultsInfo(AgentDefaults defaults);

     ResponseEntity<?> fetchingAgentDefaults(String defaults);
}
