package com.sage.api.repository.login;

import com.sage.api.domain.login.Agent;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface AgentLoginRepository {

    ResponseEntity<?> fetchingAllAgents();

    ResponseEntity<?> agentAuthentication(String name,String deviceType,String password) ;

    Agent findByUserName(String name);

    String fetchingLicense();

    Integer activeAgentCount();

    List<String> fetchingDBName(String name);
}
