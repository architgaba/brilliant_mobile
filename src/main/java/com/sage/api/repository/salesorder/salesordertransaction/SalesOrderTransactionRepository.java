package com.sage.api.repository.salesorder.salesordertransaction;

import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.salesorder.salesordertransaction.LineNotes;
import com.sage.api.domain.salesorder.salesordertransaction.StockData;
import com.sage.api.domain.salesorder.salesordertransaction.TemplateFunctionality;
import com.sage.api.domain.template.MailSender;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface SalesOrderTransactionRepository {

    ResponseEntity fetchWarehouse();

    ResponseEntity<?> fetchingItemCode();

    ResponseEntity<?> fetchingCustomerNames();

    ResponseEntity<?> fetchingCustomerDetails(String name, String docId);

    ResponseEntity<?> fetchingDocumentDetails(String name,String docID);

    ResponseEntity<?> fetchingDocumentMoreDetails(String docID);

    ResponseEntity<?> fetchingStockDetails(StockData data);

    ResponseEntity<?> deleteLine(String lineId);

    LineNotes getQtyProcessed(String lineId);

    String invoiceNumber(String accountCode);

    String orderNumber(String accountCode);

    List fetchLinesDetails(Integer documentId);

    Integer saveSignature(String signature,Integer documentId);

    Integer lineNotesUpdate(List<Integer> lineList, String [] lineNotesArray);

    String fetchingsignaturedetails(String docId);

    Integer fetchingDocumentId(String accountCode);

    void updateDocState(Integer docId);

    ResponseEntity<?> templateFunctionality(TemplateFunctionality functionality);

    ResponseEntity<?> mailSender(MailSender mailSender,String docId);

    void updateAgentIDAgainstOrder(Integer docID,Integer agentId);

    ResponseEntity<?> receiptsData(String InvoiceNum);

    ResponseEntity<?> glAccountDropDown();

    String fetchingInvoiceNum(Integer docId, String accountCode);

    String fetchingOrderNumInvoice(String accountCode);

    String docState(String documentId);

    String quoteNumber(String docId, String accountCode);

    String updatedOrderNum(String docId, String accountCode);

    ResponseEntity<?> updateCustomFields(String orderNum, String docId,AccountDetails accountDetails);

}
