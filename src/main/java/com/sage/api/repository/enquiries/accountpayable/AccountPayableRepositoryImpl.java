package com.sage.api.repository.enquiries.accountpayable;

import com.sage.api.domain.enquiries.*;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.*;

@Repository
@Transactional
public class AccountPayableRepositoryImpl implements AccountPayableRepository {
    private static final Logger log = LogManager.getLogger(AccountPayableRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * FETCHING ALL PAYABLE(SUPPLIER) ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllAccounts() {
        log.info("Entering Repository Class :::: AccountPayableRepository :::: method :::: fetchingAllAccounts");
        List<ViewObj> accountData = new ArrayList<>();
        List<ViewObj> nameData = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            List<Accounts> accountsObj = jdbcTemplate.query(SageQueries.Q_FETCHING_ACCOUNTS, new AccountsMapper());
            accountsObj.forEach(accounts -> {
                ViewObj jsonMapClass = new ViewObj();
                ViewObj MapClass = new ViewObj();
                MapClass.setValue(accounts.getName());
                MapClass.setViewValue(accounts.getAccount());
                nameData.add(MapClass);
                jsonMapClass.setValue(accounts.getAccount());
                jsonMapClass.setViewValue(accounts.getName());
                accountData.add(jsonMapClass);
            });
            jsonObject.put("accounts", accountData);
            jsonObject.put("names", nameData);
            log.info("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAllAccounts");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAllAccounts :::: Error :::::" + e);
            return ResponseDomain.internalServerError("No Supplier Exists",false);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAllAccounts :::: Error :::::" + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING ACCOUNTS(SUPPLIER) PAYABLE DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAccountData(LocalDate fromDate, LocalDate endDate, Accounts accounts) {
        log.info("Entering Repository Class :::: AccountPayableRepository :::: method :::: fetchingAccountData");
        String account = accounts.getAccount();
        String name = accounts.getName();
        float total = 0.0f, outstanding = 0.0f;
        try {
            Integer accountLink = jdbcTemplate.queryForObject(SageQueries.Q_GETTING_ACCOUNT_LINK, new Object[]{name, account}, Integer.class);
            Float dcBalance = jdbcTemplate.queryForObject(SageQueries.Q_GETTING_VENDOR_BALANCE, new Object[]{accountLink}, Float.class);
            List<AccountsData> accountData = jdbcTemplate.query(SageQueries.Q_GETTING_ACCOUNT_PAYABLE_DATA, new Object[]{accountLink, fromDate, endDate}, new AccountDataMapper());
            List<AccountsDataView> listData = new ArrayList<>();
            if (!accountData.isEmpty()) {
                for (AccountsData data : accountData) {
                    AccountsDataView viewData = new AccountsDataView();
                    viewData.setDate(data.getDate());
                    viewData.setDescription(data.getDescription());
                    viewData.setReference(data.getReference());
                    viewData.setOutstanding(String.format("%.2f", data.getOutstanding()));
                    viewData.setAmount(String.format("%.2f", data.getAmount()));
                    viewData.setExtOrderNo(data.getExtOrderNo());
                    viewData.setOrderNo(data.getOrderNo());
                    viewData.setTrCode(data.getTrCode());
                    listData.add(viewData);
                    total = total + (data.getAmount());
                    outstanding = outstanding + data.getOutstanding();
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("accountPayableData", listData);
                jsonObject.put("total", String.format("%.2f", total));
                jsonObject.put("outstanding", String.format("%.2f", outstanding));
                jsonObject.put("dcBalance", String.format("%.2f", dcBalance));
                log.info("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAccountData ");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else {
                log.info("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAccountData :::: no data found");
                return new ResponseEntity<>(new ResponseDomain( HttpStatus.OK, new Date(),"No Data Available",true), HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: AccountPayableRepository :::: method :::: fetchingAccountData :::: Error :::: "+e);
            return ResponseDomain.badRequest("Invalid Supplier Name / Code Combination",false);
        }
    }
}
