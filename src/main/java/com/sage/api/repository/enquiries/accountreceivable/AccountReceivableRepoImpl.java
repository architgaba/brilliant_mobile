package com.sage.api.repository.enquiries.accountreceivable;

import com.sage.api.domain.enquiries.ViewObj;
import com.sage.api.domain.enquiries.*;
import com.sage.api.utils.SageQueries;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Transactional
@Repository
public class AccountReceivableRepoImpl implements AccountReceivableRepo {

    private static final Logger log = LogManager.getLogger(AccountReceivableRepoImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private ResponseDomain responseDomain;

    /**
     *
     * FETCHING ALL RECEIVABLE(CLIENT) ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllAccounts() {
        log.info("Entering Repository Class :::: AccountReceivableRepoImpl :::: method :::: fetchingAllAccounts");
        List<ViewObj> accountData = new ArrayList<>();
        List<ViewObj> nameData = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            List<Accounts> accounts = jdbcTemplate.query(SageQueries.Q_FETCHING_ALL_ACCOUNTS_RECEIVABLE, new AccountsMapper());
            accounts.forEach(data ->
            {
                ViewObj jsonMapClass = new ViewObj();
                jsonMapClass.setValue(data.getAccount());
                jsonMapClass.setViewValue(data.getName());
                accountData.add(jsonMapClass);
                ViewObj jsonMapClass1 = new ViewObj();
                jsonMapClass1.setValue(data.getName());
                jsonMapClass1.setViewValue(data.getAccount());
                nameData.add(jsonMapClass1);
            });
            jsonObject.put("accounts", accountData);
            jsonObject.put("names", nameData);
            log.info("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: fetchingAllAccounts");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }catch (EmptyResultDataAccessException e){
            log.error("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: fetchingAllAccounts :::: " + e);
            return ResponseDomain.internalServerError("No Client Exists",false);
        }
        catch (Exception e) {
            log.error("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: fetchingAllAccounts :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING ACCOUNT(SUPPLIER) RECEIVABLE DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> accountReceivableData(Accounts accounts) {
        log.info("Entering Repository Class :::: AccountReceivableRepoImpl :::: method :::: accountReceivableData");
        String name = accounts.getName();
        String account = accounts.getAccount();
        Float total = 0.0f, outstanding = 0.0f;
        LocalDate startDate = LocalDate.parse(accounts.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(accounts.getToDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        try {
            Integer account_link = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DCLINK_CLIENT, new Object[]{name, account}, Integer.class);
            Float dcBalance = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_BALANCE_CLIENT, new Object[]{account_link}, Float.class);
            List<AccountsData> accountReceivableData = jdbcTemplate.query(SageQueries.Q_FETCHING_ACCOUNT_RECEIVABLE_DATA, new Object[]{account_link, startDate, endDate}, new AccountDataMapper());
            List<AccountsDataView> listData = new ArrayList<>();
            if (!accountReceivableData.isEmpty()) {
                for (AccountsData data : accountReceivableData) {
                    AccountsDataView viewData = new AccountsDataView();
                    viewData.setDate(data.getDate());
                    viewData.setDescription(data.getDescription());
                    viewData.setReference(data.getReference());
                    viewData.setOutstanding(String.format("%.2f", data.getOutstanding()));
                    viewData.setAmount(String.format("%.2f", data.getAmount()));
                    viewData.setExtOrderNo(data.getExtOrderNo());
                    viewData.setOrderNo(data.getOrderNo());
                    viewData.setTrCode(data.getTrCode());
                    listData.add(viewData);
                    total = total + (data.getAmount());
                    outstanding = outstanding + data.getOutstanding();
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("accountReceivableData", listData);
                jsonObject.put("total", String.format("%.2f", total));
                jsonObject.put("outstanding", String.format("%.2f", outstanding));
                jsonObject.put("dcBalance", String.format("%.2f", dcBalance));
                log.info("Exiting Repository Class :::: AccountPayableRepository :::: method :::: accountReceivableData ");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else {
                log.info("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: accountReceivableData ");
                return ResponseDomain.successResponse("No Data Available",true);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: accountReceivableData :::: Error :::: "+e);
            return ResponseDomain.badRequest("Invalid Client Name / Code Combination",false);
        }catch (Exception e){
            log.error("Exiting Repository Class :::: AccountReceivableRepoImpl :::: method :::: accountReceivableData :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }
}
