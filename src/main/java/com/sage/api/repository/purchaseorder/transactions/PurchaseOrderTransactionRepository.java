package com.sage.api.repository.purchaseorder.transactions;

import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.salesorder.salesordertransaction.TemplateFunctionality;
import com.sage.api.domain.template.MailSender;
import org.springframework.http.ResponseEntity;

public interface PurchaseOrderTransactionRepository {

    String purchaseOrderNumber(String supplierCode);

    String grvInvoiceNumber(String supplierCode);

    Integer saveSignature(String signature,String orderNum);

    String grvNumber(Integer docId, String supplierCode);

    Integer fetchingDocumentId(String supplierCode);

    String docState(String docId);

    String purchaseQuoteNumber(String docId, String supplierCode);

    ResponseEntity<?> glAccountDropDown();

    ResponseEntity<?> STKDropDown();

    ResponseEntity<?> supplierDetails(String name,String docId);

    ResponseEntity<?> templateFunctionality(TemplateFunctionality functionality);

    ResponseEntity<?> mailSender(MailSender mailSender, String docId);

    ResponseEntity<?> updateCustomFields(String orderNum, String docId,AccountDetails accountDetails);

    Integer updateAgentIdAgainstOrder(String docId,Integer agentId);
}
