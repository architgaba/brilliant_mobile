package com.sage.api.repository.purchaseorder.list;

import com.sage.api.domain.purchaseorder.list.PurchaseOrderList;
import com.sage.api.domain.purchaseorder.list.PurchaseOrderListData;
import com.sage.api.domain.purchaseorder.list.PurchaseOrderListDataMapper;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class PurchaseOrderListRepositoryImpl implements PurchaseOrderListRepository {
    private static final Logger log = LogManager.getLogger(PurchaseOrderListRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     *FETCHING PURCHASE ORDER DATA
     *
     * */
    @Override
    public ResponseEntity<?> fetchingPurchaseOrderListData(PurchaseOrderList purchaseOrderList) {
        log.info("Entering Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData");
        LocalDate startDate = LocalDate.parse(purchaseOrderList.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(purchaseOrderList.getToDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String docState = purchaseOrderList.getDocState();
        try {
            Integer account_link = jdbcTemplate.queryForObject(SageQueries.Q_GETTING_ACCOUNT_LINK, new Object[]{purchaseOrderList.getCustomerName(), purchaseOrderList.getAccount()}, Integer.class);
            if (docState.equalsIgnoreCase("All")) {
                List<PurchaseOrderListData> listData = jdbcTemplate.query(SageQueries.Q_FETCHING_PURCHASE_ORDER_LIST_DATA, new Object[]{account_link, startDate, endDate}, new PurchaseOrderListDataMapper());
                if (listData.isEmpty()) {
                    log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData ");
                    return ResponseDomain.successResponse("No data Available",true);
                } else {
                    for (PurchaseOrderListData data : listData) {
                        data.setAmount(Float.valueOf(String.format("%.2f", data.getAmount())));
                        data.setDocId(data.getDocId());
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("purchaseOrderListData", listData);
                    log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData ");
                    return new ResponseEntity<>(jsonObject, HttpStatus.OK);
                }
            } else if (docState.equalsIgnoreCase("Archived") || docState.equalsIgnoreCase("Quotation") || docState.equalsIgnoreCase("Partially Processed") || docState.equalsIgnoreCase("Unprocessed") || docState.equalsIgnoreCase("Archived Quotation")) {
                List<PurchaseOrderListData> listData = jdbcTemplate.query(SageQueries.Q_FETCHING_PURCHASE_ORDER_LIST_DATA_DOCSTATE, new Object[]{account_link, docState, startDate, endDate}, new PurchaseOrderListDataMapper());
                if (listData.isEmpty()) {
                    log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData ");
                    return ResponseDomain.successResponse("No data Available",true);
                } else {
                    for (PurchaseOrderListData data : listData) {
                        data.setAmount(Float.valueOf(String.format("%.2f", data.getAmount())));
                        data.setDocId(data.getDocId());
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("purchaseOrderListData", listData);
                    log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData ");
                    return new ResponseEntity<>(jsonObject, HttpStatus.OK);
                }

            } else {
                log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData ");
                return ResponseDomain.badRequest("Unknown Doc State",false);
            }

        } catch (EmptyResultDataAccessException e) {
            log.info("Exiting Repository Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingPurchaseOrderListData :::: Error :::: " + e);
            return ResponseDomain.badRequest("Invalid  Combination",false);
        }
    }
}
