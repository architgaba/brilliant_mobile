package com.sage.api.repository.purchaseorder.transactions;

import com.sage.api.domain.defaults.GLAccountsListPOFilter;
import com.sage.api.domain.defaults.GLAccountsListPOFilterMapper;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.DropDownView;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.StockPrices;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.StockPricesMapper;
import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.domain.salesorder.salesordertransaction.receipts.AccountDetailsMapper;
import com.sage.api.domain.template.MailSender;
import com.sage.api.service.template.TemplateService;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Decoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class PurchaseOrderTransactionRepositoryImpl implements PurchaseOrderTransactionRepository {

    private static final Logger log = LogManager.getLogger(PurchaseOrderTransactionRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TemplateService templateService;

    /**
     *
     * FETCHING PURCHASE ORDER NUMBER
     *
     *
     **/
    @Override
    public String purchaseOrderNumber(String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber");
        try {
            String orderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_PURCHASE_ORDER_NUM, new Object[]{supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber");
            return orderNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING MAX GRV NUMBER
     *
     *
     **/
    @Override
    public String grvInvoiceNumber(String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvInvoiceNumber");
        try {
            String grvNumber = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MAX_GRV_NUMBER, new Object[]{supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvInvoiceNumber");
            return grvNumber;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvInvoiceNumber :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING GRV NUMBER
     *
     *
     **/
    @Override
    public String grvNumber(Integer docId, String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber");
        try {
            String grvNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_GRV_NUM, new Object[]{docId,supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber");
            return grvNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING DOCUMENT ID
     *
     *
     **/
    @Override
    public Integer fetchingDocumentId(String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
        try {
            if (supplierCode != null) {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AUTO_INDEX_PURCHASE_ORDER, new Object[]{supplierCode}, Integer.class);
            } else {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return Integer.parseInt("No such Document exist");
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId :::: Error :::: " + e);
            return Integer.parseInt("" + e);
        }
    }

    /**
     *
     * FETCHING DOC STATE
     *
     *
     **/
    @Override
    public String docState(String docId) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_STATE, new Object[]{docId}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING QUOTATION NUMBER
     *
     *
     **/
    @Override
    public String purchaseQuoteNumber(String docId, String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_QUOTE_NUM, new Object[]{docId, supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber");
            return docState;
        } catch (EmptyResultDataAccessException  e){
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber");
            return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_ORDER_NUMBER,new Object[]{docId, supplierCode}, String.class);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber :::: Error :::: " + e);
            return "Error" + e;
        }
    }

    /**
     *
     * FETCHING GL DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> glAccountDropDown() {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
        try {
            List<GLAccountsListPOFilter> glFilter = jdbcTemplate.query(SageQueries.Q_FETCHING_GL_ACCOUNTS_IN_LIST_PO_FILTER, new GLAccountsListPOFilterMapper());
            List<DropDownView> code = new ArrayList<>();
            List<DropDownView> description = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();
            glFilter.forEach(data -> {
                DropDownView codeView = new DropDownView();
                DropDownView desc = new DropDownView();
                codeView.setValue(data.getMasterSubAccount());
                codeView.setViewValue(data.getDescription());
                codeView.setPrice("0.00");
                desc.setValue(data.getDescription());
                desc.setViewValue(data.getMasterSubAccount());
                desc.setPrice("0.00");
                code.add(codeView);
                description.add(desc);
            });
            jsonObject.put("itemCode", code);
            jsonObject.put("itemDescription", description);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown :::: Error :::: " + e);
            return ResponseDomain.internalServerError("GL List Not Available", false);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     *
     * FETCHING STOCK DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> STKDropDown() {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
        JSONObject jsonObject = new JSONObject();
        List<DropDownView> code = new ArrayList<>();
        List<DropDownView> description = new ArrayList<>();
        ;
        try {
            List<StockPrices> stockPrices = jdbcTemplate.query(SageQueries.Q_FETCHING_STOCK_PRICES_PO, new StockPricesMapper());
            stockPrices.forEach(data -> {
                DropDownView codeView = new DropDownView();
                DropDownView descView = new DropDownView();
                codeView.setValue(data.getStkCode());
                codeView.setViewValue(data.getStkDescription());
                codeView.setPrice(String.format("%.2f", data.getStkCost()));
                descView.setValue(data.getStkDescription());
                descView.setViewValue(data.getStkCode());
                descView.setPrice(String.format("%.2f", data.getStkCost()));
                code.add(codeView);
                description.add(descView);
            });
            jsonObject.put("itemCode", code);
            jsonObject.put("itemDescription", description);
            log.info("Exiting Repository Class ::::  PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown :::: Error :::: "+e);
            return ResponseDomain.internalServerError("No STK Available",false);
        } catch(Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING DEFAULT DATA FOR MAIL
     *
     *
     **/
    @Override
    public ResponseEntity<?> mailSender(MailSender mailSender, String reference) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: mailSender");
        String docId = "";
        try {
            docId = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_ORDER_BASIS, new Object[]{reference}, String.class);
            if (docId == null)
                docId = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_INV_BASIS, new Object[]{reference}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: mailSender");
            return templateService.mailSenderForPurchaseOrder(mailSender, docId);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: mailSender :::: Error :::: " + e);
            return ResponseDomain.badRequest("Bad Request No Doc ID Found related to that Document", false);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: mailSender :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     *
     * TEMPLATE FUNCTIONALITY(DOWNLOAD/PREVIEW)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(TemplateFunctionality functionality) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
        try {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_INV, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download")) {
                return templateService.downloadPdfForPurchaseOrder(details.getDocId(), details.getDocState());
            } else if (functionality.getAction().equalsIgnoreCase("View")) {
                return templateService.viewTemplateHtmlForPurchaseOrder(details.getDocId(), details.getDocState());
            } else {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return ResponseDomain.badRequest("Undefined Action",false);
            }
        } catch (EmptyResultDataAccessException e) {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_ORD, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download"))
                return templateService.downloadPdfForPurchaseOrder(details.getDocId(), details.getDocState());
            else if (functionality.getAction().equalsIgnoreCase("View"))
                return templateService.viewTemplateHtmlForPurchaseOrder(details.getDocId(),details.getDocState());
            else {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return new ResponseEntity<>(new ResponseDomain(true, "Undefined Action"), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception e){
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: templateFunctionality :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING SUPPLIER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> supplierDetails(String name, String docId) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
        JSONObject jsonObject = new JSONObject();
        try {
            if (name != null && docId == null || docId.equals("0")) {
                AccountDetails supplier =  jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_SUPPLIER_DETAILS_NEW, new Object[]{name}, new AccountDetailsMapper());
                jsonObject.put("supplier", supplier);
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else if (docId != null) {
                AccountDetails supplier =  jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_EXISTING_SUPPLIER_DETAILS, new Object[]{docId}, new AccountDetailsExistingMapper());
                jsonObject.put("supplier", supplier);
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
                return new ResponseEntity<>(supplier, HttpStatus.OK);
            }else {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
                return ResponseDomain.internalServerError("Server Error",false);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
            return ResponseDomain.badRequest("No Such Supplier exists",false);
        }
    }

    @Override
    public ResponseEntity<?> updateCustomFields(String orderNum, String docId, AccountDetails accountDetails) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateCustomFields");
        String purchaseOrderNum ="";
        try {
            if (docId != null) {
                purchaseOrderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM_ON_DOCID_BASIS, new Object[]{docId}, String.class);
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
            } else if (!orderNum.startsWith("P")) {
                purchaseOrderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM_ON_INVNUMBER_BASIS, new Object[]{orderNum}, String.class);
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), purchaseOrderNum});
            } else {
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
            }
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateCustomFields");
            return new ResponseEntity(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "CustomFields Saved Successfully", true), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateCustomFields :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error in Saving Custom Fields", false);
        }
    }

    @Override
    public Integer updateAgentIdAgainstOrder(String docId,Integer agentId) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateAgentIdAgainstOrder");
        String purchaseOrderNum ="";
        try{
            purchaseOrderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM_ON_DOCID_BASIS, new Object[]{docId}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateAgentIdAgainstOrder");
            return  jdbcTemplate.update(SageQueries.Q_UPDATING_AGENT_ID_AGAINST_ORDER_PURCHASE_ORDER, new Object[]{agentId, purchaseOrderNum});
        }catch (Exception e){
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: updateAgentIdAgainstOrder :::: Error :::: "+e);
            return 0;
        }
    }

    @Override
    public Integer saveSignature(String signature, String orderNum) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: saveSignature");
        byte[] imageBytes = null;
        try {
            signature = signature.substring(signature.indexOf(",") + 1);
            BASE64Decoder decoder = new BASE64Decoder();
            imageBytes = decoder.decodeBuffer(signature);
            int i = jdbcTemplate.update(SageQueries.Q_UPDATING_SIGNATURE_PURCHASE_ORDER, new Object[]{imageBytes, orderNum});
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: saveSignature");
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: saveSignature :::: Error :::: " + e);
            return 0;
        }
    }
}
