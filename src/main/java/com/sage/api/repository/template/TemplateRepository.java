package com.sage.api.repository.template;

import com.sage.api.domain.template.SavedTemplate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TemplateRepository extends CrudRepository<SavedTemplate, Long> {

    List<SavedTemplate> findByDocState(String docState);

    List<SavedTemplate> findByDocId(Long docId);

}
