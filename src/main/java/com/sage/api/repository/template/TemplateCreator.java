package com.sage.api.repository.template;

import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import com.sage.api.domain.template.TemplateLines;
import com.sage.api.domain.template.TemplateOrderTotals;
import com.sage.api.domain.template.TemplatePOJO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TemplateCreator {

    TemplatePOJO templateData(String docId);

    TemplatePOJO templateSupplierData(String docId);

    TemplateOrderTotals templateOrderData(String docState, String docId);

    List<TemplateLines> templateLinesData(String docId, String repCode, String docState);

    ResponseEntity<?> defaultMail(String agentName,String customer,String docState, String orderNum);

    ResponseEntity<?> defaultSupplierMail(String agentName,String customer);

    String docState(String docId);

    AccountDetails customerDetails(String name);

    String fetchingSignatureDetails(String docId);
}
