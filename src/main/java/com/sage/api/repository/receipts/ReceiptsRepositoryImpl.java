package com.sage.api.repository.receipts;

import com.sage.api.domain.enquiries.AccountDataMapper;
import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.domain.enquiries.AccountsData;
import com.sage.api.domain.receipts.CustomerAccount;
import com.sage.api.domain.receipts.CustomerAccountMapper;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;;
import java.util.List;

@Repository
public class ReceiptsRepositoryImpl implements ReceiptsRepository {

    private static final Logger log = LogManager.getLogger(ReceiptsRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * FETCHING ALL CUSTOMER ACCOUNT DC BALANCE
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchCustomerAccount() {
        log.info("Entering Repository Class :::: ReceiptsRepositoryImpl :::: method :::: fetchCustomerAccount");
        JSONObject jsonObject = new JSONObject();
        try {
            List<CustomerAccount> customerAccount = jdbcTemplate.query(SageQueries.Q_FETCHING_CUSTOMER_DC_BALANCE, new CustomerAccountMapper());
            jsonObject.put("customer", customerAccount);
            log.info("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: fetchCustomerAccount");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: fetchCustomerAccount :::: Error :::: " + e);
            return ResponseDomain.internalServerError("No Client Account Available",false);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: fetchCustomerAccount :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * UPDATING AGENT ID IN POSTAR TABLE
     *
     *
     **/
    @Override
    public String updatingPOSTAR(Integer agentId, String customerCode) {
        log.info("Entering Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingPOSTAR");
        try {
            jdbcTemplate.update(SageQueries.Q_UPDATING_AGENT_ID_POSTAR, new Object[]{agentId, customerCode});
            log.info("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingPOSTAR");
            return "success";
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingPOSTAR :::: " + e);
            return "error " + e;
        }
    }

    /**
     *
     * UPDATING POSAmntTendered/POSChange IN POSTAR TABLE
     *
     *
     **/
    @Override
    public String updatingInvNum(String tenderAmount, String changeAmount, String invoiceNum) {
        log.info("Entering Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingInvNum");
        try {
            jdbcTemplate.update(SageQueries.Q_UPDATING_INVNUM_POS, new Object[]{tenderAmount, changeAmount, invoiceNum});
            log.info("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingInvNum");
            return "success";
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: updatingInvNum :::: Error :::: " + e);
            return "error : " + e;
        }
    }

    /**
     *
     * FETCHING AGENT RECEIPTS DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> agentReceiptsData(Accounts accounts, String agent) {
        log.info("Entering Repository Class :::: ReceiptsRepositoryImpl :::: method :::: agentReceiptsData");
        LocalDate startDate = LocalDate.parse(accounts.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(accounts.getToDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        if(accounts.getName()==null || accounts.getName()=="")
            return ResponseDomain.badRequest("Select a Customer", false);
        try {
            List<AccountsData> receipts = jdbcTemplate.query(SageQueries.Q_FETCHING_RECEIPTS_DATA_AGENT, new Object[]{accounts.getName(), accounts.getAccount(), startDate, endDate, agent}, new AccountDataMapper());
            if(receipts.isEmpty()){
                log.info("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: agentReceiptsData");
                return ResponseDomain.successResponse("No Data Available",true);
            }
            log.info("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: agentReceiptsData");
            return new ResponseEntity<>(receipts, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: agentReceiptsData :::: Error :::: " + e);
            return ResponseDomain.badRequest("Invalid Combination", false);
        }catch (Exception e) {
            log.error("Exiting Repository Class :::: ReceiptsRepositoryImpl :::: method :::: agentReceiptsData :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

}
