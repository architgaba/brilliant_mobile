package com.sage.api.repository.documentation;


import com.sage.api.domain.documentation.Parameters;
import com.sage.api.domain.documentation.Result;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultsRepo extends CrudRepository<Result,Long> {

    Result findByQuickLinksRes_LinkId(@Param("linkId") Long linkId);
}
