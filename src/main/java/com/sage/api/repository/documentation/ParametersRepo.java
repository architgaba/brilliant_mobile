package com.sage.api.repository.documentation;


import com.sage.api.domain.documentation.Parameters;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ParametersRepo extends CrudRepository<Parameters,Long> {

    //@Query("SELECT * FROM parameters where parametersId=:parametersId")
    Parameters findByQuickLinksParam_LinkId(@Param("linkId") Long linkId);
}
