package com.sage.api.repository.documentation;

import java.util.List;
import com.sage.api.domain.documentation.QuickLinks;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuickLinksRepo extends CrudRepository<QuickLinks,Long> {

   List<QuickLinks> findByCategory(String description);

   @Query(value="SELECT DISTINCT category FROM quicklinks" ,nativeQuery = true)
   public List<String> findCategory();

   QuickLinks findByLinkId(Long id);
}
