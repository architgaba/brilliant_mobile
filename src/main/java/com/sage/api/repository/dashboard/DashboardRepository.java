package com.sage.api.repository.dashboard;

import org.springframework.http.ResponseEntity;

public interface DashboardRepository {

    ResponseEntity<?> data(Integer id,String agentName);

    ResponseEntity<?> loggingOut(Integer id , String deviceType);
}
