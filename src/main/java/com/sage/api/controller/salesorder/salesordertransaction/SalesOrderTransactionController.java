package com.sage.api.controller.salesorder.salesordertransaction;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.domain.template.MailSender;
import com.sage.api.repository.enquiries.accountreceivable.AccountReceivableRepoImpl;
import com.sage.api.repository.enquiries.inventory.InventoryRepositoryImpl;
import com.sage.api.service.enquiries.inventory.InventoryServiceImpl;
import com.sage.api.service.salesorder.salesordertransaction.SalesOrderTransactionService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/salesordertransaction")
public class SalesOrderTransactionController {
    private static final Logger log = LogManager.getLogger(SalesOrderTransactionController.class);
    @Autowired
    private SalesOrderTransactionService salesOrderTransactionService;
    @Autowired
    private InventoryRepositoryImpl inventoryRespository;
    @Autowired
    private InventoryServiceImpl inventoryService;
    @Autowired
    private AccountReceivableRepoImpl accountReceivableImpl;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API TO FETCH WAREHOUSE LIST
     *
     *
     **/
    @GetMapping("/warehouselist")
    ResponseEntity<?> wareHouseList() {
        log.info("Entering Controller Class ::::  SalesOrderTransactionController :::: method :::: wareHouseList");
        return salesOrderTransactionService.fetchWarehouse();
    }

    /**
     *
     * API TO GET STOCK ITEM CODE AND DESCRIPTION
     *
     *
     **/
    @GetMapping("/itemcode")
    public ResponseEntity<?> gettingItemCode() {
        log.info("Entering Controller CLass :::: SalesOrderTransactionController :::: method :::: gettingItemCode");
        return salesOrderTransactionService.fetchingItemCode();
    }


    /**
     *
     * API FOR FETCHING CUSTOMER NAME
     *
     *
     **/
    @GetMapping("/customername")
    ResponseEntity<?> customers() {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: customers");
        return salesOrderTransactionService.fetchCustomers();
    }

    /**
     *
     * API TO PLACE NEW SALES ORDER
     *
     *
     **/
    @PostMapping("/createplacesalesorder")
    ResponseEntity<?> createPlaceSalesOrder(@RequestHeader("Authorization") String authKey, @RequestBody WrapperQuotes wrapperQuotes) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: createPlaceSalesOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        SalesOrder createPlaceSalesOrder = wrapperQuotes.getQuotes().getQuote();
        if (createPlaceSalesOrder != null)
            return salesOrderTransactionService.createPlaceSalesOrder(wrapperQuotes, authKey);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: createPlaceSalesOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }


    /**
     *
     * API FOR FETCHING LINES OF A DOCUMENT
     *
     *
     **/
    @GetMapping("/fetchinglinesdetails")
    ResponseEntity<?> fetchingLinesDetails(@RequestHeader("Authorization") String authKey, @RequestParam(value = "docId", required = true) String docId) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: fetchingLinesDetails :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (docId != null)
            return salesOrderTransactionService.fetchingLinesDetails(docId, authKey);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: fetchingLinesDetails :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR FETCHING CUSTOMER DETAILS
     *
     *
     **/
    @PostMapping("/gettingcustomertdetails")
    public ResponseEntity<?> fetchCustomerDetails(@RequestParam(value = "name") String name, @RequestParam(value = "docId", required = false) String docId) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: fetchCustomerDetails");
        if (name != null)
            return salesOrderTransactionService.fetchingCustomerDetails(name, docId);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: fetchCustomerDetails");
        return ResponseDomain.badRequest("Request Parameters Missing", false);
    }

    /**
     *
     * API FOR FETCHING DOCUMENT DETAILS
     *
     *
     **/
    @PostMapping("/gettingdocumentdetails")
    public ResponseEntity<?> fetchDocumentDetails(@RequestParam(value = "name",required = false) String name,@RequestParam(value = "docId",required = false) String docId) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: fetchDocumentDetails");
        if (docId != null || name!=null)
            return salesOrderTransactionService.fetchDocumentDetails(name,docId);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: fetchDocumentDetails");
        return ResponseDomain.badRequest("Request Parameters Missing", false);
    }

    /**
     *
     * API FOR FETCHING DOCUMENT MORE DETAILS
     *
     *
     **/
    @PostMapping("/gettingdocumentmoredetails")
    public ResponseEntity<?> fetchDocumentMoreDetails(@RequestParam(value = "docId") String docId) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: fetchDocumentMoreDetails");
        if (docId != null)
            return salesOrderTransactionService.fetchMoreDocumentDetails(docId);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: fetchDocumentMoreDetails");
        return ResponseDomain.badRequest("Request Parameters Missing", false);
    }
    /**
     *
     * API FOR FETCHING STOCK DETAILS
     *
     *
     **/
    @PostMapping("/fetchingstockdetails")
    public ResponseEntity<?> stockDetails(@RequestBody StockData stockData) {
        log.info("Entering Controller CLass :::: SalesOrderTransactionController :::: method :::: stockDetails");
        if (stockData != null)
            return salesOrderTransactionService.fetchingStockData(stockData);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: stockDetails");
        return ResponseDomain.badRequest("Request Body Missing",false);
    }

    /**
     *
     * API TO UPDATE EXISTING SALES ORDER
     *
     *
     **/
    @PostMapping("/updateexistingorder")
    ResponseEntity<?> updateExitingOrder(@RequestHeader("Authorization") String authKey, @RequestBody WrapperSalesorder wrapperSalesorder) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: updateExitingOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        SalesOrder salesOrder = wrapperSalesorder.getOrders().getOrder();
        if (salesOrder != null)
            return salesOrderTransactionService.updateExistingOrder(wrapperSalesorder, authKey);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: updateExitingOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Parameters Missing", false);
    }

    /**
     *
     * API FOR INVOICING EXISTING SALES ORDER/QUOTE
     *
     *
     **/
    @PostMapping("/updatesalesorderinvoice")
    ResponseEntity<?> updateExitingSalesOrder(@RequestHeader("Authorization") String authKey, @RequestBody WrapperOrder wrapperOrder) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: updateExitingSalesOrder :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        SalesOrder orderROSalesOrder = wrapperOrder.getOrders().getOrder();
        if (orderROSalesOrder != null)
            return salesOrderTransactionService.updateExitingSalesOrder(wrapperOrder, authKey);
        log.info("Exiting :::: SalesOrderTransactionController:::: method:::: updateExitingSalesOrder :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API TO CREATE NEW INVOICE
     *
     *
     **/
    @PostMapping("/createinvoice")
    ResponseEntity<?> createInvoice(@RequestHeader("Authorization") String authKey, @RequestBody WrapperQuotes wrapperQuotes) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: createInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        SalesOrder createPlaceSalesOrder = wrapperQuotes.getQuotes().getQuote();
        if (createPlaceSalesOrder != null)
            return salesOrderTransactionService.createInvoice(wrapperQuotes, authKey);
        log.info("Exiting :::: SalesOrderTransactionController:::: method :::: createInvoice :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR CREATING QUOTE
     *
     *
     **/
    @PostMapping("/createSalesOrderQuote")
    ResponseEntity<?> salesOrderQuote(@RequestHeader("Authorization") String authKey, @RequestBody WrapperQuotes wrapperQuotes) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: createSalesOrderQuote :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        SalesOrder salesOrderQuote = wrapperQuotes.getQuotes().getQuote();
        if (salesOrderQuote != null)
            return salesOrderTransactionService.createSalesOrderQuotes(wrapperQuotes, authKey);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: createSalesOrderQuote  :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR DELETING A LINE OF A DOCUMENT
     *
     *
     **/
    @GetMapping("/deleteLines")
    public ResponseEntity<?> deletingLines(@RequestParam(value = "lineId", required = true) String lineId) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: deletingLines");
        if (lineId != null)
            return salesOrderTransactionService.deleteLine(lineId);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: deletingLines");
        return ResponseDomain.badRequest("LineID Missing", false);
    }

    /**
     *
     *  API FOR TEMPLATE FUNCTIONALITY(DOWNLOADING / VIEWING)
     *
     *
     **/
    @PostMapping("/templatefunctionatlity")
    public ResponseEntity<?> templateFunctionality(@RequestParam(name = "reference") String reference, @RequestParam(name = "action") String action) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: templateFunctionality");
        if (reference != null && action != null)
            return salesOrderTransactionService.templateFunctionality(reference, action);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: templateFunctionality");
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR MAILING TEMPLATE
     *
     *
     **/
    @PostMapping("/maildocument/{reference}")
    public ResponseEntity<?> mailSender(@RequestBody MailSender mailData, @PathVariable String reference) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: mailSender");
        if(mailData.getFrom()==null || mailData.getFrom()=="")
            return ResponseDomain.internalServerError("Agent's email address is blank.",false);
        if (mailData != null && reference != null)
            return salesOrderTransactionService.templateMailer(mailData, reference);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: mailSender");
        return ResponseDomain.badRequest("Empty RequestBody Found / No Path Variable",false);
    }

    /**
     *
     * API FOR FETCHING RECEIPTS DEFAULT DATA
     *
     *
     **/
    @GetMapping("/receiptsdata/{invNumber}")
    public ResponseEntity<?> receiptsData(@PathVariable(required = true) String invNumber) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController :::: method :::: receiptsData");
        if (invNumber != null)
            return salesOrderTransactionService.receiptsData(invNumber);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: invoicePayment");
        return ResponseDomain.badRequest("No Request Parameters Found",false);
    }

    /**
     *
     * API FOR FETCHING FINANCIAL LINES DROPDOWN(GL)
     *
     *
     **/
    @GetMapping("/gldropdown")
    public ResponseEntity<?> fetchingGLDropDownValue( ) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: fetchingGLDropDownValue");
        return salesOrderTransactionService.salesOrderGLDropdown();
    }

    /**
     *
     * API FOR SAVING/UPDATING CLIENT DETAILS
     *
     *
     **/
    @PostMapping(value = {"/updateCustomFields/{orderNum}","/updateCustomFields/docId/{docId}"})
    public ResponseEntity<?> updateCustomerDetails(@PathVariable(required = false,value = "orderNum") String orderNum,@PathVariable(required = false,value = "docId") String docId,@RequestBody AccountDetails accountDetails) {
        log.info("Entering Controller Class :::: SalesOrderTransactionController:::: method :::: updateCustomerDetails");
        if(accountDetails!=null)
            return salesOrderTransactionService.updateCustomFields(orderNum,docId,accountDetails);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController:::: method :::: updateCustomerDetails");
        return ResponseDomain.badRequest("Request Body Missing",false);
    }
}
