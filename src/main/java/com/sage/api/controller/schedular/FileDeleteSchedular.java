package com.sage.api.controller.schedular;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * SCHEDULAR FOR DELETING TEMPLATES/LOGS FROM SERVER
 **/
@Component
public class FileDeleteSchedular {
    private static final Logger logger = LogManager.getLogger(FileDeleteSchedular.class);
    private JdbcTemplate jdbcTemplate;

    @Scheduled(cron = "0 0 0 ? * 1") // EVERY SUNDAY AT MIDNIGHT
    public void scheduleTaskWithFixedRate() {
        logger.info("Executing delete file Schedular " + (LocalDateTime.now()));
        String htmlFilepath = System.getProperty("user.dir") + "\\generatedHTMl";
        String logsPath = System.getProperty("user.dir") + "\\brilliantMobile_logs";
        File folder = new File(htmlFilepath);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile())
                    listOfFiles[i].delete();
            }
        }
        String pdfFilepath = System.getProperty("user.dir") + "\\generatedPDF";
        File pdfFolder = new File(pdfFilepath);
        File[] listOfPDFFiles = pdfFolder.listFiles();
        if (listOfPDFFiles != null) {
            for (int i = 0; i < listOfPDFFiles.length; i++) {
                if (listOfPDFFiles[i].isFile())
                    logger.info("File " + listOfPDFFiles[i].delete());
            }
        }
        jdbcTemplate.execute("truncate table template");
        File logFolder = new File(logsPath);
        File[] logFilesList = logFolder.listFiles();
        if (logFilesList != null) {
            for (int i = 0; i < logFilesList.length; i++) {
                if (!(logFilesList[i].getName().equalsIgnoreCase("current_logs.log"))) {
                    LocalDate logFileDate = LocalDate.parse((logFilesList[i].getName()).substring(0, 10), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                    LocalDate currentDate = LocalDate.now().minusDays(2);
                    if (logFileDate.isBefore(currentDate))
                        logFilesList[i].delete();
                }
            }
        }
    }
}


