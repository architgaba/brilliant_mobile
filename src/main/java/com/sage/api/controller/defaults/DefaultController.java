package com.sage.api.controller.defaults;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.defaults.AgentDefaults;
import com.sage.api.service.defaults.DefaultsService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/defaults")
public class DefaultController {
    private static final Logger log = LogManager.getLogger(DefaultController.class);
    @Autowired
    private DefaultsService defaultsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /*
     *
     * API FOR FETCHING DEFAULTS DATA
     *
     *
     */
    @GetMapping("/data")
    public ResponseEntity<?> defaultData(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: DefaultController :::: method :::: defaultData :::: Agent : "+jwtTokenUtil.getUsernameFromToken(authKey));
        return defaultsService.defaultData();
    }

    /*
     *
     * API FOR SAVING/UPDATING AGENTS DEFAULTS
     *
     *
     */
    @PostMapping("/saveuserdefaults")
    public ResponseEntity<?> savingAgentDefaultData(@RequestHeader("Authorization") String authKey, @RequestBody AgentDefaults agentdefaults) {
        log.info("Entering Controller Class :::: DefaultController :::: method :::: savingAgentDefaultData :::: Agent : " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (agentdefaults != null)
            return defaultsService.saveAgentDefaults(agentdefaults);
        log.info("Exiting Controller Class :::: DefaultController :::: method :::: savingAgentDefaultData");
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /*
     *
     * API FOR RETRIEVING AGENT DEFAULTS
     *
     *
     */
    @GetMapping("/getagentdefaults/{agentName}")
    public  ResponseEntity<?> agentDefaults(@RequestHeader("Authorization") String authKey,@PathVariable String agentName){
        log.info("Entering Controller Class :::: DefaultController :::: method :::: agentDefaults :::: Agent : "+jwtTokenUtil.getUsernameFromToken(authKey));
        if (agentName!=null)
            return defaultsService.getAgentDefaults(agentName);
        log.info("Exiting Controller Class :::: DefaultController :::: method :::: agentDefaults");
        return ResponseDomain.badRequest("Request Parameters Missing", false);
    }
}
