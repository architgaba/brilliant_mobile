package com.sage.api.controller.enquiries.accountreceivable;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.service.enquiries.accountreceivable.AccountReceivableService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/enquiries")
public class AccountReceivableController {

    private static final Logger log = LogManager.getLogger(AccountReceivableController.class);
    @Autowired
    private AccountReceivableService receivableService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR FETCHING ALL RECEIVABLE(CLIENT) ACCOUNTS
     *
     *
     **/
    @GetMapping("/accountreceivable")
    ResponseEntity<?> getAccounts(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: AccountReceivableController :::: method :::: getAccounts");
        return receivableService.fetchingAllAccounts();
    }


    /**
     *
     * API FOR FETCHING ACCOUNTS(CLIENT) RECEIVABLE DATA
     *
     *
     **/
    @PostMapping("/gettingaccountreceivabledata")
    ResponseEntity<?> accountReceivableData(@RequestHeader("Authorization") String authKey, @RequestBody Accounts accounts) {
        log.info("Entering Controller Class :::: AccountReceivableController :::: method :::: accountReceivableData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (accounts.getName() != null || accounts.getToDate() != null || accounts.getAccount() != null || accounts.getFromDate() != null)
            return receivableService.accountReceivableData(accounts);
        log.info("Exiting Controller Class :::: AccountReceivableController :::: method :::: accountReceivableData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }
}