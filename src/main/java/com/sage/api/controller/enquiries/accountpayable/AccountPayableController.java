package com.sage.api.controller.enquiries.accountpayable;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.service.enquiries.accountpayable.AccountPayableService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/enquiries")
public class AccountPayableController {
    private static final Logger log = LogManager.getLogger(AccountPayableController.class);
    @Autowired
    private AccountPayableService accountPayableService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR FETCHING ALL PAYABLE(SUPPLIER) ACCOUNTS
     *
     *
    **/
    @GetMapping("/accountpayable")
    ResponseEntity<?> getAccounts(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: AccountPayableController :::: method :::: getAccounts :::: Agent");
        return accountPayableService.fetchingAllAccounts();
    }

    /**
     *
     * API FOR FETCHING ACCOUNT(SUPPLIER) PAYABLE DATA
     *
     *
     **/
    @PostMapping("/accountpayable/data")
    ResponseEntity<?> getAccountData(@RequestHeader("Authorization") String authKey, @RequestBody Accounts accounts) {
        log.info("Entering Controller Class :::: AccountPayableController :::: method :::: getAccountData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (accounts.getName() != null || accounts.getToDate() != null || accounts.getAccount() != null || accounts.getFromDate() != null)
            return accountPayableService.fetchingAccountData(accounts);
        log.info("Exiting Controller Class :::: AccountPayableController :::: method :::: getAccountData Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing",false);
    }
}


