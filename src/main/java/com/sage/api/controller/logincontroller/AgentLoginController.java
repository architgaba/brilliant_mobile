package com.sage.api.controller.logincontroller;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.login.ForgotPassword;
import com.sage.api.domain.login.LoggedUser;
import com.sage.api.domain.login.User;
import com.sage.api.service.login.AgentLoginService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
public class AgentLoginController {
    private static final Logger log = LogManager.getLogger(AgentLoginController.class);
    @Autowired
    private AgentLoginService agentService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    /**
     *
     * API FOR FETCHING ALL AGENTS
     *
     *
     **/
    @GetMapping("/agents")
    ResponseEntity<?> getAgents() {
        log.info("Entering Controller Class :::: AgentLoginController :::: method :::: getAgents");
        return agentService.getAgents();
    }

    /**
     *
     * API FOR AGENT AUTHENTICATION
     *
     *
     **/
    @PostMapping("/authenticate")
    ResponseEntity<?> agentAuthentication(@RequestBody User user) {
        log.info("Entering Controller Class :::: AgentLoginController :::: method :::: agentAuthentication");
        if (user != null)
            return agentService.agentAuthentication(user.getUserName(), user.getPassword(), user.getDeviceType());
        log.info("Exiting Controller Class :::: AgentLoginController :::: method :::: agentAuthentication");
        return ResponseDomain.badRequest("Request Body Missing",false);
    }

    /**
     *
     * API FOR FETCHING LOGGED IN AGENT
     *
     *
     **/
    @GetMapping("/loggendinagent")
    ResponseEntity fetchLoggedInAgent(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: AgentLoginController :::: method :::: fetchLoggedInAgent");
        LoggedUser user= new LoggedUser(jwtTokenUtil.getUsernameFromToken(authKey));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        log.info("Exiting Controller Class :::: AgentLoginController :::: method :::: fetchLoggedInAgent");
        return new ResponseEntity(user,responseHeaders,HttpStatus.OK);
    }

    /**
     *
     * API FOR FORGOT PASSWORD
     *
     *
     **/
    @PostMapping(value = "/forgotpassword")
    ResponseEntity<?> forgotPassword(@RequestBody ForgotPassword agentDetails){
        log.info("Entering Controller Class :::: AgentLoginController :::: method :::: forgotPassword :::: Agent");
        return agentService.resetPassword(agentDetails);
    }

}
