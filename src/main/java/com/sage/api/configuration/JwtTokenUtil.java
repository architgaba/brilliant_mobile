package com.sage.api.configuration;

import com.sage.api.domain.login.Agent;
import com.sage.api.utils.JwtConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;


@Component
public class JwtTokenUtil {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final Logger log = LogManager.getLogger(JwtTokenUtil.class);
    private static String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKAUZV+tjiNBKhlBZbKBnzeugpdYPhh5PbHanjV0aQ+LF7vetPYhbTiCVqA3a+Chmge44+prlqd3qQCYra6OYIe7oPVq4mETa1c/7IuSlKJgxC5wMqYKxYydb1eULkrs5IvvtNddx+9O/JlyM5sTPosgFHOzr4WqkVtQ71IkR+HrAgMBAAECgYAkQLo8kteP0GAyXAcmCAkA2Tql/8wASuTX9ITD4lsws/VqDKO64hMUKyBnJGX/91kkypCDNF5oCsdxZSJgV8owViYWZPnbvEcNqLtqgs7nj1UHuX9S5yYIPGN/mHL6OJJ7sosOd6rqdpg6JRRkAKUV+tmN/7Gh0+GFXM+ug6mgwQJBAO9/+CWpCAVoGxCA+YsTMb82fTOmGYMkZOAfQsvIV2v6DC8eJrSa+c0yCOTa3tirlCkhBfB08f8U2iEPS+Gu3bECQQCrG7O0gYmFL2RX1O+37ovyyHTbst4s4xbLW4jLzbSoimL235lCdIC+fllEEP96wPAiqo6dzmdH8KsGmVozsVRbAkB0ME8AZjp/9Pt8TDXD5LHzo8mlruUdnCBcIo5TMoRG2+3hRe1dHPonNCjgbdZCoyqjsWOiPfnQ2Brigvs7J4xhAkBGRiZUKC92x7QKbqXVgN9xYuq7oIanIM0nz/wq190uq0dh5Qtow7hshC/dSK3kmIEHe8z++tpoLWvQVgM538apAkBoSNfaTkDZhFavuiVl6L8cWCoDcJBItip8wKQhXwHp0O3HLg10OEd14M58ooNfpgt+8D8/8/2OOFaR0HzA+2Dm";
    private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgFGVfrY4jQSoZQWWygZ83roKXWD4YeT2x2p41dGkPixe73rT2IW04glagN2vgoZoHuOPqa5and6kAmK2ujmCHu6D1auJhE2tXP+yLkpSiYMQucDKmCsWMnW9XlC5K7OSL77TXXcfvTvyZcjObEz6LIBRzs6+FqpFbUO9SJEfh6wIDAQAB";

    /**
     *
     * GENERATING TOKEN FOR NEW USER
     *
     *
     **/
    public String generateToken(Agent agent, String deviceType,String password) {
        return doGenerateToken(agent.getCAgentName(), agent.getIdAgents(), deviceType,password);
    }

    /**
     *
     * GENERATING TOKEN FOR NEW USER
     *
     *
     **/
    private String doGenerateToken(String subject, int agentId, String deviceType,String password) {
        Claims claims = Jwts.claims().setSubject(subject);
        String encryptedPass = Base64.getEncoder().encodeToString(encrypt(password, publicKey));
        storePassword(encryptedPass,agentId);
        String userId = String.valueOf(agentId);
        claims.put("deviceType", deviceType);
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer("https://brilliantlink.co.za")
                .setAudience(userId)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JwtConstants.ACCESS_TOKEN_VALIDITY_SECONDS * 100))
                .signWith(SignatureAlgorithm.HS256, JwtConstants.SIGNING_KEY)
                .compact();
        return token;
    }

    /*
     * FETCHING AGENT NAME FROM TOKEN
     */
    public String getUsernameFromToken(String token) {
        String key = token.replace(JwtConstants.TOKEN_PREFIX, "");
        return getClaimFromToken(key, Claims::getSubject);
    }

    /**
     *
     * FETCHING TOKEN EXPIRY DATE FROM TOKEN
     *
     *
     **/
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     *
     * FETCHING CLAIMS(PAYLOAD) FROM TOKEN
     *
     *
     **/
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     *
     * FETCHING CLAIMS FROM TOKEN
     *
     *
     **/
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     *
     * VALIDATING TOKEN
     *
     *
     **/
    public Boolean validateToken(String token, Agent userDetails) {
        final String username = getUsernameFromToken(token);
        String deviceType = getDeviceTypeFromToken(token);
        String query = "select " + deviceType + " from agent_login_status where name=?";
        String validToken = jdbcTemplate.queryForObject(query, new Object[]{username}, String.class);
        if(validToken.equals("0"))
            return false;
        else if (checkPass(token,validToken) && username.equals(userDetails.getCAgentName()) && !isTokenExpired(token))
            return true;
        else
            return false;
    }


    /**
     *
     * VALIDATING TOKEN EXPIRY
     *
     *
     **/
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     *
     * FETCHING DEVICE FROM TOKEN
     *
     *
     **/
    public String getDeviceTypeFromToken(String token) {
        String finalToken = token.replace(JwtConstants.TOKEN_PREFIX, "");
        Claims claims = Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(finalToken)
                .getBody();
        String deviceType = claims.get("deviceType", String.class);
        return deviceType;
    }

    /**
     *
     * FETCHING AGENT PASSWORD FROM TOKEN
     *
     *
     **/
    public String getPasswordFromToken(String token) {
        try {
            String username = getUsernameFromToken(token);
            String storedPassword=jdbcTemplate.queryForObject("Select password from agent_login_status where name= ?",new Object[]{username},String.class);
            String decryptedString = decrypt(storedPassword, privateKey);
            return decryptedString;
        }catch (Exception e){
            return null;
        }
    }

    /**
     *
     * FETCHING AGENT ID FROM TOKEN
     *
     *
     **/
    public Integer getAgentIdFromToken(String token) {
        String finalToken = token.replace(JwtConstants.TOKEN_PREFIX, "");
        Claims claims = Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(finalToken)
                .getBody();
        String id = claims.getAudience();
        int agentID = Integer.parseInt(id);
        return agentID;
    }

    /**
     *
     * VALIDATING TOKEN
     *
     *
     **/
    private boolean checkPass(String plainPassword, String hashedPassword) {
        if (BCrypt.checkpw(plainPassword, hashedPassword))
            return true;
        else
            return false;
    }

    /**
     *
     * ENCRYPTING PASSWORD
     *
     *
     **/
    public static byte[] encrypt(String data, String publicKey) {
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
            return cipher.doFinal(data.getBytes());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * FETCHING PublicKey FOR PASSWORD ENCRYPTION
     *
     *
     **/
    public static PublicKey getPublicKey(String base64PublicKey) {
        PublicKey publicKey = null;
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    /**
     *
     * FETCHING PrivateKey FOR PASSWORD DECRYPTION
     *
     *
     **/
    public static PrivateKey getPrivateKey(String base64PrivateKey) {
        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    /**
     *
     * DECRYPTING PASSWORD
     *
     *
     **/
    public static String decrypt(byte[] data, PrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(data));
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            return null;
        }
    }

    public static String decrypt(String data, String base64PrivateKey) {
        return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
    }

    /**
     *
     * STORING PASSWORD IN DB
     *
     *
     **/
    private void storePassword(String pass,int id){
        try{
            jdbcTemplate.update("update agent_login_status set password=? where id=?",new Object[]{pass,id});
        }catch (Exception e){
            log.error("Error in Saving Agent Password :::: Error "+e);
        }
    }
}
