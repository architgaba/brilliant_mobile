package com.sage.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * EXPOSING API ON SWAGGER
 *
 *
 **/
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("BrilliantLink API Documentation")
                //.description("Build By Nile Technologies Pvt. ltd.")
              //  .description("Nile Technologies offers technology solutions and has proven Expertise in Oracle Fusion Middleware. Nile has experience in Application Development, Application Integration, Project Management, Reports, Mobile App and Portals in various leading vertical such as BPO, Telecom, Logistics, Travel and Tourism, Manufacturing, Energy and Utilities, Public Sector and FSI.")
                .version("1.0.0")
                .contact(new Contact("Nile Technologies Pvt. Ltd.", "http://www.niletechnologies.com", "enquiry@niletechnologies.com"))
                .build();
    }

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sage.api"))
                .build()
                .apiInfo(apiInfo());
    }
}
