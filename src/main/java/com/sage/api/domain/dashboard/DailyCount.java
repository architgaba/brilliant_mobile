package com.sage.api.domain.dashboard;

import lombok.Data;

import java.io.Serializable;

@Data
public class DailyCount implements Serializable {
    private Integer day;
    private Float total;
}
