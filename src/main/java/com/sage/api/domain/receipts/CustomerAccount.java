package com.sage.api.domain.receipts;

import lombok.Data;
import java.io.Serializable;

@Data
public class CustomerAccount implements Serializable{
    private String name;
    private String account;
    private float balance;
}
