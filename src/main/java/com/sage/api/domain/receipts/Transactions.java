package com.sage.api.domain.receipts;

import com.sage.api.domain.salesorder.salesordertransaction.Receipts;
import lombok.Data;

import java.io.Serializable;

@Data
public class Transactions implements Serializable{
    private Receipts transac;
}
