package com.sage.api.domain.login;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoggedUser implements Serializable {
    private String agentName;
    public LoggedUser(String agentName) {
        this.agentName = agentName;
    }
}
