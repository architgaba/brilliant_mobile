package com.sage.api.domain.salesorder.stockcheck;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import  com.sage.api.domain.salesorder.stockcheck.StockCheck;

public class StockCheckMapper implements RowMapper<StockCheck> {

    @Override
    public StockCheck mapRow(ResultSet resultSet, int i) throws SQLException {
        StockCheck stockCheck = new StockCheck();

        stockCheck.setWhcode(resultSet.getString("WhCode"));
        stockCheck.setWhdescription(resultSet.getString("Name"));
        stockCheck.setQty_SO(resultSet.getFloat("WHQtyOnSO"));
        stockCheck.setQty_PO(resultSet.getFloat("WHQtyOnPO"));
        stockCheck.setQty_Hand(resultSet.getFloat("WHQtyOnHand")-resultSet.getFloat("WHJobQty")-resultSet.getFloat("WHMFPQty")-resultSet.getFloat("WHQtyReserved"));
        return stockCheck;
    }
}
