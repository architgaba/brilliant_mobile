package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;
import java.io.Serializable;

@Data
public class StockDetailsData  implements Serializable{

    private String itemCode;

    private String itemDescription;

    private String warehouseCode;

    private String priceListName;

    private float exclusivePrice;

    private float inclusivePrice;

    private Integer TaxRate;

    private Integer TaxCode;
}
