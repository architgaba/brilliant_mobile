package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class Quote implements Serializable{
    private SalesOrder quote;
}
