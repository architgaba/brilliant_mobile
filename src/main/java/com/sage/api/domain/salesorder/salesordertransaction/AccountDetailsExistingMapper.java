package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDetailsExistingMapper implements RowMapper<AccountDetails> {
    @Override
    public AccountDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        AccountDetails accountDetails = new AccountDetails();
        accountDetails.setContactPerson(resultSet.getString("cContact"));
        accountDetails.setAddress1(resultSet.getString("Address1"));
        accountDetails.setAddress2(resultSet.getString("Address2"));
        accountDetails.setAddress3(resultSet.getString("Address3"));
        accountDetails.setAddress4(resultSet.getString("Address4"));
        accountDetails.setAddress5(resultSet.getString("Address5"));
        accountDetails.setAddress6(resultSet.getString("Address6"));
        accountDetails.setPAddress1(resultSet.getString("PAddress1"));
        accountDetails.setPAddress2(resultSet.getString("PAddress2"));
        accountDetails.setPAddress3(resultSet.getString("PAddress3"));
        accountDetails.setPAddress4(resultSet.getString("PAddress4"));
        accountDetails.setPAddress5(resultSet.getString("PAddress5"));
        accountDetails.setPAddress6(resultSet.getString("PAddress6"));
        accountDetails.setTelephone(resultSet.getString("cTelephone"));
        accountDetails.setEmail(resultSet.getString("cEmail"));
        return accountDetails;
    }
}

