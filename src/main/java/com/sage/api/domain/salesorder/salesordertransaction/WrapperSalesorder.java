package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

@Data
public class WrapperSalesorder implements Serializable{
    private SalesOrderRO orders;
    //private String [] linenotes;
    private Map<String,String> linenotes;
    private String signature;
}
