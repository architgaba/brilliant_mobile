package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MoreDocumentDetailsMapper implements RowMapper<MoreDocumentDetails> {
    @Override
    public MoreDocumentDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        MoreDocumentDetails documentDetails = new MoreDocumentDetails();
     /*   documentDetails.setImage();
        documentDetails.setLotNumber();
        documentDetails.setModule_ST_OR_GL();*/
        documentDetails.setProjectCode(resultSet.getString("ProjectCode"));
        documentDetails.setRepCode(resultSet.getString("RepCode"));
        return documentDetails;
    }
}
