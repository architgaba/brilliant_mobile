package com.sage.api.domain.salesorder.salesordertransaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Hash implements Serializable {
    @JsonProperty
    private String Key;
    @JsonProperty
    private String Value;
}
