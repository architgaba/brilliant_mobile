package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class WrapperQuotes implements Serializable{
    private Quote quotes;
    //private String [] linenotes;
    private Map<String,String> linenotes;
    private String signature;
}
