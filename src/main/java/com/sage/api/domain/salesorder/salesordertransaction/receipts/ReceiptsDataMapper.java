package com.sage.api.domain.salesorder.salesordertransaction.receipts;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReceiptsDataMapper implements RowMapper<ReceiptsData> {
    @Override
    public ReceiptsData mapRow(ResultSet resultSet, int i) throws SQLException {
        ReceiptsData receiptsData = new ReceiptsData();
        receiptsData.setAmount(resultSet.getString("InvTotIncl"));
        receiptsData.setCustomerCode(resultSet.getString("Account"));
        receiptsData.setOrderNum(resultSet.getString("OrderNum"));
        receiptsData.setCustomerName(resultSet.getString("Name"));
        return receiptsData;
    }
}
