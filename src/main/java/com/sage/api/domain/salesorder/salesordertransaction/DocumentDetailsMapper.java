package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentDetailsMapper implements RowMapper<DocumentDetails> {

    @Override
    public DocumentDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        DocumentDetails documentDetails = new DocumentDetails();
        documentDetails.setDeliveryMethod(resultSet.getString("DeliveryMethod"));
        documentDetails.setDueDate(resultSet.getDate("Duedate"));
        documentDetails.setProjectCode(resultSet.getString("ProjectCode"));
        documentDetails.setRepCode(resultSet.getString("RepCode"));
        documentDetails.setOrderStatus(resultSet.getString("DocStateDescription"));
        return documentDetails;
    }
}
