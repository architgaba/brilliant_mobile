package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;
import java.io.Serializable;

@Data
public class AccountDetails implements Serializable {
private String Address1;
private String Address2;
private String Address3;
private String Address4;
private String Address5;
private String Address6;
private String PAddress1;
private String PAddress2;
private String PAddress3;
private String PAddress4;
private String PAddress5;
private String PAddress6;
private String telephone;
private String contactPerson;
private String email;
}
