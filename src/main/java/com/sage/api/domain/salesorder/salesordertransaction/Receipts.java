package com.sage.api.domain.salesorder.salesordertransaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Receipts implements Serializable{
    @JsonProperty("Date")
    private String date;
    @JsonProperty("Description")
    private String Description;
    @JsonProperty("OrderNum")
    private String OrderNum;
    @JsonProperty("Reference")
    private String Reference;
    @JsonProperty("Reference2")
    private String Reference2;
    @JsonProperty("TransactionCode")
    private String TransactionCode;
    @JsonProperty("Amount")
    private float Amount;
    @JsonProperty("ExchangeRate")
    private float ExchangeRate;
    @JsonProperty("ForeignAmount")
    private float ForeignAmount;
    @JsonProperty("OverrideCreditAccountCode")
    private String OverrideCreditAccountCode;
    @JsonProperty("OverrideDebitAccountCode")
    private String OverrideDebitAccountCode;
    @JsonProperty("TaxRateCode")
    private String  TaxRateCode;
    @JsonProperty("CustomerCode")
    private String CustomerCode;
    @JsonProperty("ProjectCode")
    private String ProjectCode;
    @JsonProperty("RepCode")
    private String RepCode;

}
