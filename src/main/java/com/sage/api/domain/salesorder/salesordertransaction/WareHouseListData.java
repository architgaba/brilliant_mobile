package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class WareHouseListData implements Serializable{
    private String code;
    private String name;
}
