package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocDetailsMapper implements RowMapper<DocDetails>{

    @Override
    public DocDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        DocDetails details = new DocDetails();
        details.setDocId((resultSet.getString("AutoIndex")));
        details.setDocState(resultSet.getString("DocStateDescription"));
        return  details;
    }
}
