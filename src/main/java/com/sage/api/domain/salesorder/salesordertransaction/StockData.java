package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;
import java.io.Serializable;

@Data
public class StockData implements Serializable{
    private String customerName;
    private String stockCode;
    private String whCode;
}


