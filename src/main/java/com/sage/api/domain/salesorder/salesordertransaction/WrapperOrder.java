package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class WrapperOrder implements Serializable{
    private OrderRO orders;
   // private String [] linenotes;
   private Map<String,String> linenotes;
    private String signature;
}
