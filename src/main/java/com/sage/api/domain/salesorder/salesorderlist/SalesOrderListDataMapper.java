package com.sage.api.domain.salesorder.salesorderlist;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SalesOrderListDataMapper implements RowMapper<SalesOrderListData> {

    @Override
    public SalesOrderListData mapRow(ResultSet resultSet, int i) throws SQLException {
        SalesOrderListData salesOrderListData= new SalesOrderListData();
        salesOrderListData.setDocState(resultSet.getString("DocStateDescription"));
        salesOrderListData.setReference(resultSet.getString("InvNumber"));
        salesOrderListData.setClientAccount(resultSet.getString("Account"));
        salesOrderListData.setClientName(resultSet.getString("Name"));
        salesOrderListData.setOrderNo(resultSet.getString("OrderNum"));
        salesOrderListData.setExtOrderNo(resultSet.getString("ExtOrderNum"));
        salesOrderListData.setOrderDate(resultSet.getDate("OrderDate"));
        salesOrderListData.setDueDate(resultSet.getDate("DueDate"));
        salesOrderListData.setInvDate(resultSet.getDate("InvDate"));
        salesOrderListData.setRepCode(resultSet.getString("RepCode"));
        salesOrderListData.setUser(resultSet.getString("INVNUMAgentName"));
        salesOrderListData.setOrderStatus(resultSet.getString("StatusDescrip"));
        salesOrderListData.setAmount(resultSet.getFloat("OrdTotIncl"));
        salesOrderListData.setDocId(resultSet.getLong("AutoIndex"));
        return salesOrderListData;
    }
}
