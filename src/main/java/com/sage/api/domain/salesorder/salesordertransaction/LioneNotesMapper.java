package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LioneNotesMapper implements RowMapper<LineNotes> {
    @Override
    public LineNotes mapRow(ResultSet resultSet, int i) throws SQLException {
        LineNotes lineNotes = new LineNotes();
        lineNotes.setQty(resultSet.getInt("fQtyProcessed"));
        lineNotes.setLineNotes(resultSet.getString("cLineNotes"));
        lineNotes.setFUnitPriceIncl(resultSet.getFloat("fUnitPriceIncl"));
        lineNotes.setTaxRate(resultSet.getInt("fTaxRate"));
        return lineNotes;
    }
}
