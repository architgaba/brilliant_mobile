package com.sage.api.domain.salesorder.salesordertransaction;

import com.sage.api.domain.salesorder.salesorderlist.SalesOrderListData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WareHouseListDataMapper implements RowMapper<WareHouseListData> {
    @Override
    public WareHouseListData mapRow(ResultSet resultSet, int i) throws SQLException {
        WareHouseListData wareHouseListData = new WareHouseListData();
        wareHouseListData.setCode(resultSet.getString("Code"));
        wareHouseListData.setName(resultSet.getString("Name"));
        return wareHouseListData;
    }
}
