package com.sage.api.domain.defaults;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DataViewObj implements Serializable {
    private List<String>  warehouseCode;
    private List<String>  repCode;
    private List<String>  projectCode;
    private List<String>  salesOrderAcc;
    private List<String>  purchaseOrderAcc;
    private List<String>  till;
    private List<POSPaymentTransactionCode> pspm;
    private List<GLAccountsListSOFilter> glAccountsSOListFilter;
    private List<GLAccountsListPOFilter> glAccountsPOListFilter;

}
