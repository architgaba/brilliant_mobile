package com.sage.api.domain.defaults;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GLAccountsListPOFilterMapper implements RowMapper<GLAccountsListPOFilter> {
    @Override
    public GLAccountsListPOFilter mapRow(ResultSet resultSet, int i) throws SQLException {
        GLAccountsListPOFilter listPOFilter = new GLAccountsListPOFilter();
        listPOFilter.setMasterSubAccount(resultSet.getString("Master_Sub_Account"));
        listPOFilter.setDescription(resultSet.getString("Description"));
        return  listPOFilter;

    }
}
