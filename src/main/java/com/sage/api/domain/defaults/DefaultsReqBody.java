package com.sage.api.domain.defaults;

import lombok.Data;

import java.io.Serializable;

@Data
public class DefaultsReqBody implements Serializable{
    private String agentName;
    private String whCode;
    private boolean whFilter;
    private String project;
    private boolean projectFilter;
    private boolean repFilter;
    private String repCode;
    private String salesOrderAccCode;
    private boolean salesOrderAccFilter;
    private String purchaseOrderAccCode;
    private boolean purchaseOrderAccFilter;
    private String till;
    private boolean tillFilter;
    private POSPaymentTransactionCode pspm;
    private boolean pspmFilter;
}
