package com.sage.api.domain.defaults;

import lombok.Data;

import java.io.Serializable;

@Data
public class POSPaymentTransactionCode implements Serializable{
    private String code;
    private String Description ;
}
