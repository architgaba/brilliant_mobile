package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;


@Data
public class InventoryViewObj implements Serializable{
    private java.util.Date Date;
    private String TxCode;
    private String Reference;
    private String Description;
    private String OrderNo;
    private String ExtOrderNo;
    private String Amount;
    private String Qty;
    private String WH;
    private String Account;
    private String RepCode;

}

