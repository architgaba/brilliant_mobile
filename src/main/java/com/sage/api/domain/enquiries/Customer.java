package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;


@Data
public class Customer implements Serializable{

    private String account;
    private String name;
    private int dcLink;

}