package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AccountsData implements Serializable{

    private Date Date;
    private String TrCode;
    private String Reference;
    private String Description;
    private String OrderNo;
    private String ExtOrderNo;
    private float Amount ;
    private float Outstanding;

}
