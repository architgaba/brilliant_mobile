package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;

@Data
public class ViewObj implements Serializable{
    private String viewValue;
    private String value;
}
