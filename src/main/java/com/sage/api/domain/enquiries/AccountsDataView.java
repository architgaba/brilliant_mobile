package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountsDataView implements Serializable{

    private java.util.Date Date;
    private String TrCode;
    private String Reference;
    private String Description;
    private String OrderNo;
    private String ExtOrderNo;
    private String Amount;
    private String Outstanding;

}
