package com.sage.api.domain.enquiries;

import lombok.Data;

import java.io.Serializable;

@Data
public class Accounts  implements Serializable{

    private String account;
    private String name;
    private String fromDate;
    private String toDate;
}
