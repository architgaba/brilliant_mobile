package com.sage.api.domain.enquiries;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountsMapper implements RowMapper<Accounts> {

    @Override
    public Accounts mapRow(ResultSet resultSet, int i) throws SQLException {
        Accounts accounts =new Accounts();
        accounts.setAccount(resultSet.getString("Account"));
        /*accounts.setAccountLink(resultSet.getLong("DCLink"));*/
        accounts.setName(resultSet.getString("Name"));
        return accounts;
    }
}
