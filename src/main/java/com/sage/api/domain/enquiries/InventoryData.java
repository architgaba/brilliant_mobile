package com.sage.api.domain.enquiries;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class InventoryData implements Serializable{

    private Date Date;
    private String TxCode;
    private String Reference;
    private String Description;
    private String OrderNo;
    private String ExtOrderNo;
    private float Amount;
    private float Qty;
    private String WH;
    private String Account;
    private String RepCode;
}
