package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ExistingOrder implements Serializable {
    @JsonProperty
    private int DocumentID;
    @JsonProperty
    private String DueDate;
    @JsonProperty
    private String ExternalOrderNo;
    @JsonProperty
    private String InvoiceDate;
    @JsonProperty
    private String OrderDate;
    @JsonProperty
    private String OrderNo;
    @JsonProperty
    private String OrderPriority;
    @JsonProperty
    private String OrderStatus;
    @JsonProperty
    private String ProjectCode;
    @JsonProperty
    private String SettlementTerm;
    @JsonProperty
    private String SupplierAccountCode;
    @JsonProperty
    private String SupplierInvoiceNo;
    @JsonProperty
    List<com.sage.api.domain.purchaseorder.purchaseordertransaction.ExistingLines> Lines;
    @JsonProperty
    ArrayList<com.sage.api.domain.purchaseorder.purchaseordertransaction.ExistingFinancialLines> FinancialLines;
    @JsonProperty
    List<com.sage.api.domain.salesorder.salesordertransaction.Hash> Hash;
}
