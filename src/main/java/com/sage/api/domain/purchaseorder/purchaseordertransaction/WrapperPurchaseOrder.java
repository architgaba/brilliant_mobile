package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class WrapperPurchaseOrder implements Serializable{
    private PurchaseOrderRO purchaseOrderRO;
    private String [] lineNotes;
    private String signature;
}
