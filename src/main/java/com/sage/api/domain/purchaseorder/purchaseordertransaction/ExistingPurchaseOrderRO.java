package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;
import java.io.Serializable;

@Data
public class ExistingPurchaseOrderRO implements Serializable{
    private ExistingOrder order;
}
