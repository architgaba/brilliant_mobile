package com.sage.api.domain.purchaseorder.list;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PurchaseOrderListDataMapper implements RowMapper<PurchaseOrderListData> {

    @Override
    public PurchaseOrderListData mapRow(ResultSet resultSet, int i) throws SQLException {
        PurchaseOrderListData purchaseOrderListData= new PurchaseOrderListData();
        purchaseOrderListData.setDocState(resultSet.getString("DocStateDescription"));
        purchaseOrderListData.setReference(resultSet.getString("InvNumber"));
        purchaseOrderListData.setOrderNo(resultSet.getString("OrderNum"));
        purchaseOrderListData.setOrderDate(resultSet.getDate("OrderDate"));
        purchaseOrderListData.setDueDate(resultSet.getDate("DueDate"));
        purchaseOrderListData.setInvDate(resultSet.getDate("InvDate"));
        purchaseOrderListData.setUser(resultSet.getString("INVNUMAgentName"));
        purchaseOrderListData.setOrderStatus(resultSet.getString("OrderStatus"));
        purchaseOrderListData.setAmount(resultSet.getFloat("OrdTotIncl"));
        purchaseOrderListData.setDocId(resultSet.getLong("AutoIndex"));
        purchaseOrderListData.setFlag(resultSet.getString("Flag"));
        return  purchaseOrderListData;
    }
}
