package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class WrapperExistingPuchaseOrder implements Serializable {
    private ExistingPurchaseOrderRO purchaseOrderRO;
    private String [] lineNotes;
    private String signature;
}
