package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateOrderTotals implements Serializable{

    private String totalExc;
    private String tax;
    private String totalIncl;
    private String discount;
    private String totalInclFinal;
}
