
package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailSender implements Serializable{

    private String from;
    private String[] to;
    private String[] cc;
    private String[] bcc;
    private String body;
    private String subject;
    private String fileName;
}
