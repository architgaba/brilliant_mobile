package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Lines implements Serializable{
    private List<String> fieldNames;
    private List<TemplateLines> tableRow;

}
