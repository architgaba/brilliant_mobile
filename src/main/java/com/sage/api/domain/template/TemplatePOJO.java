package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TemplatePOJO implements Serializable{
    private String customerName;
    private String telephone;
    private String contactPerson;
    private String pa1;
    private String pa2;
    private String pa3;
    private String pa4;
    private String po1;
    private String po2;
    private String po3;
    private String fax;
    private String email1;
    private String account;
    private String date;
    private String orderNo;
    private String reference;
    private String repCode;
    private String docState;
    private String totalExc;
    private String tax;
    private String totalIncl;
    private String discount;
    private String totalInclFinal;
    private String currentdate;
    private String signature;
    private Lines lines;

}
