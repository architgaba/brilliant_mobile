package com.sage.api.domain.template;

import lombok.Data;
import java.io.Serializable;

@Data
public class ReceiptData implements Serializable{
    private String account;
    private String customerName;
    private String date;
    private String till;
    private String description;
    private String amountPaid;
    private String cash;
    private String card;
    private String eft;
    private String other;
    private String total;
    private String balanceAmount;
    private String changeAmount;
}
