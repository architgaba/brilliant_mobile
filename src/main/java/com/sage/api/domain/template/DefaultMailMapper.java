package com.sage.api.domain.template;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DefaultMailMapper implements RowMapper<DefaultMail> {
    @Override
    public DefaultMail mapRow(ResultSet resultSet, int i) throws SQLException {
        DefaultMail mail = new DefaultMail();
        mail.setSentTo(resultSet.getString("Email"));;
        return mail;
    }
}
