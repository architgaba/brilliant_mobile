package com.sage.api.domain.template;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TemplateLinesMapper implements RowMapper<TemplateLines> {

    @Override
    public TemplateLines mapRow(ResultSet resultSet, int i) throws SQLException {
        TemplateLines templateLines= new TemplateLines();
        templateLines.setItemDescription(resultSet.getString("cDescription"));
        templateLines.setExclPrice(String.valueOf(resultSet.getFloat("fUnitPriceExcl")));
        templateLines.setInclPrice(String.valueOf(resultSet.getFloat("fQuantityLineTotIncl")));
        templateLines.setTax(String.valueOf(resultSet.getFloat("fQuantityLineTaxAmount")));
        templateLines.setDiscount(String.valueOf(resultSet.getFloat("fLineDiscount")));
        templateLines.setLineNotes(resultSet.getString("cLineNotes"));
        templateLines.setQtyOrdered(resultSet.getInt("fQuantity"));
        templateLines.setQtyProcessed(resultSet.getInt("fQtyProcessed"));
        templateLines.setQtyToInvoice(resultSet.getInt("fQtyToProcess"));
        return templateLines;
    }
}
