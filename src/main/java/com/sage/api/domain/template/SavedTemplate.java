package com.sage.api.domain.template;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "template")
@Component
public class SavedTemplate implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long templateId;

    private Long docId;


    private String docState;

    @Column(columnDefinition="TEXT")
    private String template;


}
