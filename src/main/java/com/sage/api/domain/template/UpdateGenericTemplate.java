package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateGenericTemplate implements Serializable {
    String docState;
    String template;
}
