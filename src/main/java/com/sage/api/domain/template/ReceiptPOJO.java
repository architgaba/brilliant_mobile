package com.sage.api.domain.template;

import lombok.Data;

@Data
public class ReceiptPOJO {
    private String pa1;
    private String pa2;
    private String pa3;
    private String pa4;
    private String po1;
    private String po2;
    private String po3;
    private String telephone;
    private String email1;
    private String contactPerson;
    private String account;
    private String customerName;
    private String date;
    private String till;
    private String description;
    private String amountPaid;
    private String amountRemaining;
    private String cash;
    private String card;
    private String eft;
    private String other;
    private String total;
    private String balanceAmount;
    private String changeAmount;
}
