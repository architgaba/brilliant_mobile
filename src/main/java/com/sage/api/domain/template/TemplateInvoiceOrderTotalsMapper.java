package com.sage.api.domain.template;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TemplateInvoiceOrderTotalsMapper implements RowMapper<TemplateOrderTotals> {

    @Override
    public TemplateOrderTotals mapRow(ResultSet resultSet, int i) throws SQLException {
        TemplateOrderTotals orderTotals = new TemplateOrderTotals();
        orderTotals.setTax(resultSet.getString("InvTotTax"));
        orderTotals.setTotalIncl(resultSet.getString("InvTotIncl"));
        orderTotals.setTotalExc(resultSet.getString("InvTotExcl"));
        orderTotals.setTotalIncl(resultSet.getString("InvTotIncl"));
        orderTotals.setDiscount(resultSet.getString("InvDiscAmnt"));
        return orderTotals;
    }
}
