package com.sage.api.domain.documentation;

import lombok.Data;

@Data
public class ResponseColumn {

    private String name;

    private String type;

    private String description;

}
