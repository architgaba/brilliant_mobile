package com.sage.api.domain.documentation;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name="quicklinks")
@ToString(exclude = {"result","parameters"})
public class QuickLinks implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long linkId;

    @OneToOne(cascade = CascadeType.ALL,targetEntity = Result.class,mappedBy ="quickLinksRes")
    private Result result;

    @OneToOne(cascade = CascadeType.ALL,targetEntity = Parameters.class,mappedBy = "quickLinksParam")
    private Parameters parameters;

    private String endPoint;

    private String description;

    private String category;

    private String url;

    private String methods;



}