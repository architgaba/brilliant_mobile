package com.sage.api.domain.documentation;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class ResponseTable implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String type;
    private String description;
    public ResponseTable(){}
    public ResponseTable(String name, String type, String description) {
        this.name = name;
        this.type = type;
        this.description = description;
    }
}
