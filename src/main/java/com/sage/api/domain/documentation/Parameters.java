package com.sage.api.domain.documentation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "parameters")
@ToString(exclude = "quickLinksParam")
public class Parameters  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long parametersId;

    @OneToOne(fetch =FetchType.LAZY ,optional = false)
    @JoinColumn(name="linkId",nullable = false)
    private QuickLinks quickLinksParam;
    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "linkId")
    List<ResponseTable> responseTable;
}
