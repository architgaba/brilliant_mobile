package com.sage.api.domain.documentation;


import lombok.Data;

import java.util.List;

@Data
public class ParameterResultVO {
    private Parametersvo parametersvo;
    private Long quickLinkId;
    private ResultVO resultVO;
    private String url;
    private String methods;

}
