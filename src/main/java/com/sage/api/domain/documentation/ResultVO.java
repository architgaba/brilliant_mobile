package com.sage.api.domain.documentation;


import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
public class ResultVO implements Serializable {
    ArrayList<ResponseTable> responseTable;

}
