package com.sage.api.domain.documentation;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
public class ResponsesTable implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String type;
    private String description;
    public ResponsesTable(){}
    public ResponsesTable(String name, String type, String description) {
        this.name = name;
        this.type = type;
        this.description = description;
    }
}